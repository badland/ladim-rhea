module grid

  use setup
  !use ncutil, only : nf_open, nf_close, check_err, dimlen, getvar,   &
  !                   NF_NOWRITE
  use ncutil

  implicit none

  integer :: imax  ! Number of grid cells in X-direction
  integer :: jmax  ! Number of grid cells in Y-direction
  integer :: kmax  ! Number of sigma levels
  real, dimension(:,:), allocatable   :: H, mask_rho
  real, dimension(:,:), allocatable   :: lon_rho, lat_rho, angle
  real, dimension(:,:), allocatable   :: pm, pn
  real, dimension(:,:), allocatable   :: mask_u, mask_v
  real, dimension(:,:,:), allocatable :: depth, depth_w

  public initgrid

  contains
  !
  ! ****************************************************
  !
  subroutine initgrid(filename,lon_filename)  

    character(len=*), intent(in) :: filename
    character(len=*), intent(in) :: lon_filename

    integer :: ncid      ! NetCDF file identifier
    integer :: ncid_lon  ! NetCDF file for lon_rho and lat_rho
    integer :: status    ! NetCDF error status
    integer :: status_lon    ! NetCDF error status

    real, dimension(:), allocatable :: sc_r, cs_r
    real, dimension(:), allocatable :: sc_w, cs_w
    real :: hc
    integer :: k
    

    print*,filename
    ! Open the file
    status = nf90_open(filename, NF90_NOWRITE, ncid)
    call check_err(status)

    ! Open the file
   print *, "Open his file", lon_filename 
    status_lon = nf90_open(lon_filename, NF90_NOWRITE, ncid_lon)
   call check_err(status_lon)
 
    ! Get the problem size
    imax = dimlen(ncid, "xi_rho") - 2
    print *, "imax = ", imax
    jmax = dimlen(ncid, "eta_rho") - 2
    print *, "jmax = ", jmax 
    kmax = dimlen(ncid_lon, "s_rho")
    print *, "kmax = ", kmax
    
    ! Allocate the arrays
    allocate(H(0:imax+1, 0:jmax+1), mask_rho(0:imax+1, 0:jmax+1))
    allocate(lon_rho(0:imax+1, 0:jmax+1), lat_rho(0:imax+1, 0:jmax+1))
    allocate(angle(0:imax+1, 0:jmax+1))
    allocate(pm(0:imax+1, 0:jmax+1), pn(0:imax+1, 0:jmax+1))
    allocate(mask_u(1:imax+1, 0:jmax+1))
    allocate(mask_v(0:imax+1, 1:jmax+1))

    allocate(sc_r(kmax), cs_r(kmax))
    allocate(sc_w(0:kmax), cs_w(0:kmax))
    allocate(depth(0:imax+1, 0:jmax+1, kmax))
    allocate(depth_w(0:imax+1, 0:jmax+1, 0:kmax))

    ! Read the 2D arrays
    call getvar(ncid, "h",        H)
    call getvar(ncid, "mask_rho", mask_rho)
    call getvar(ncid, "lon_rho",  lon_rho)
    call getvar(ncid, "lat_rho",  lat_rho)
    call getvar(ncid, "angle",    angle)
    call getvar(ncid, "pm",       pm)
    call getvar(ncid, "pn",       pn)
    call getvar(ncid, "mask_u",   mask_u)
    call getvar(ncid, "mask_v",   mask_v)

    ! Read values for computing the vertical structure
    ! OBS, kan lett beregne sc
!    call getvar(ncid, "sc_r", sc_r)
    call getvar(ncid_lon, "s_rho", sc_r)
    call getvar(ncid_lon, "Cs_r", cs_r)
    ! Sometimes the sc_w-arrays has only kmax points

    if (dimlen(ncid_lon, "s_w") == kmax+1) then
!      call getvar(ncid, "sc_w", sc_w)
      call getvar(ncid_lon , "s_w", sc_w)
      call getvar(ncid_lon, "Cs_w", cs_w)
    else  ! only kmax points
!      call getvar(ncid, "sc_w", sc_w(1:kmax))
      call getvar(ncid_lon, "s_w", sc_w(1:kmax))
      call getvar(ncid_lon, "Cs_w", cs_w(1:kmax))
    end if 
    sc_w(0) = -1.0
    cs_w(0) = -1.0
    sc_w(kmax) = 0.0
    cs_w(kmax) = 0.0
    call getvar(ncid_lon, "hc", hc)

    ! Compute the vertical coordinate structure
    ! k = 1 er ved botnen
    do k = 1, kmax
      depth(:,:,k)   = hc * sc_r(k) + (H-hc) * cs_r(k)
      depth_w(:,:,k) = hc * sc_w(k) + (H-hc) * cs_w(k)
    end do
    depth_w(:,:,0)    = - H
    !
    ! Clean up
    ! 
   print *, "GRID.F90 FERDIG" 
    status = nf90_close(ncid)
    status = nf90_close(ncid_lon)
  end subroutine initgrid

end module grid
