module setup

  use time_module

  implicit none

  logical, parameter, private :: DEBUG = .TRUE.
  integer, parameter, private :: SUPFIL = 17

! <jonal>: Utgangspunktet er � sette tidsskrittet slik at typisk avlagt driftsavstand
!          ikke overstiger en gitterlengde per tidssteg, dvs. DT < DX/U.
!          Eks.: DX=200m og U=0.1m/s => DT < 2000s
 
!  integer, parameter :: dt = 3600  ! 1 hour time step
!  integer, parameter :: dt = 7200  ! 2 hour time step
!  integer, parameter :: dt = 1800  ! 1/2 hour time step  ! For test
!  integer, parameter :: dt = 900   ! 15 min time step
!  integer, parameter :: dt = 300   ! 5 min time step
!   integer, parameter :: dt = 180   ! 3 min time step      !for 200 m grid
!   integer, parameter :: dt = 120   ! 2 min time step      !for 160 m grid
   integer, parameter :: dt = 300   ! 2 min time step      !for 160 m grid
!  integer, parameter :: dt = 600    ! 10 min for 800 m grid

  type(time_type) :: start_time
  type(time_type) :: stop_time

  integer :: nstep

!  integer :: istart
!  integer :: antdag
!  real    :: vekt
  ! f� bedre navn under
  character(len=160) :: bfnavn  ! Name of bottom file.
  character(len=160) :: fname  ! Name of input NetCDF file
  character(len=80) :: PartFileName ! Name of particle init file
  character(len=80) :: utfnavn ! Name of output file     

  integer :: start_fnum = 0  ! Number of first input file

  integer :: outper   !  Output frequency
  integer :: Nout     !  Number of output instances

  integer :: Nvarout  ! Number of output variables
  integer, dimension(:), allocatable :: outvar
                      ! Output variables


  contains

! -----------------
  subroutine readsup(supfilename)
! -----------------

    character(len=*), intent(in) :: supfilename

    character(len=80) linje
    integer, dimension(6) :: timevec
    integer :: daystep
    integer :: simdays
    integer :: dtout

    integer :: i

!
!***FIRST EXECUTABLE STATEMENT LESSUP
!
!    if (DEBUG) write(*,*) 'READSUP: Started'
  if (DEBUG) write(*,*) 'READSUP: supfile = ', supfilename

    print *, "time step = ", dt, " seconds"
    if (mod(86400, dt) /= 0) then
      print *, "Time step is not dividing 24 hours"
      print *, "Exiting LADIM"
      stop
    end if
    daystep = 86400 / dt
    print *, "daystep = ", daystep


!    open(SUPFIL, file='ladim.sup', status='old', action='read') 
    open(SUPFIL, file=supfilename, status='old', action='read')
    if (DEBUG) write(*,*) 'READSUP: Opened set-up file'
!
    ! Simulation starting time
    call readln(linje) 
    start_time = linje
!FV    print *, "start_time = ", timestr(start_time)
    print *, "start_time = ", start_time
    
!
    call readln(linje) 
    read(linje,*) simdays
    write(*,*) ' Simulation time ', simdays, ' days'
    stop_time = start_time + simdays
!FV    print *, "stop_time = ", timestr(stop_time)
    print *, "stop_time = ", stop_time

    ! Burde sjekket: dt deler d�gn
    nstep = simdays * daystep
    print *, "nstep = ", nstep

! --- Files.

    call readln(linje)      ! Bottom matrix.
    read(linje,*) bfnavn
    write(*,*) 'Bunnfil : ', bfnavn

    call readln(linje)      ! Input file.
    read(linje,*) fname
    print *, fname
    if (index(fname, '0001.nc')  > 0) then
!FV    if (index(fname, '000.nc')  > 0) then
      read(linje, *) fname, start_fnum
      write(*,*) 'Input file name pattern : ', fname
      write(*,*) "Start file number = ", start_fnum
    else
      write(*,*) 'Input file name : ', fname
    end if



    call readln(linje)      ! Particle init file
    read(linje, *) PartFileName
    write(*,*) "Particle init file : ", PartFileName

! tidevannsfil, coming later


    call readln(linje)      ! Output file.
    read(linje,*) utfnavn
    write(*,*) 'Utfil   : ', utfnavn

!    call readln(linje)      ! Particle release time step.
!    read(linje,*) dtpar
!    dtpar = dtpar * 3600    
!    write(*,*) 'Partikkel tidssteg  :', dtpar, ' sekunder'      

    call readln(linje)      ! Output time step.
    read(linje,*) dtout
    dtout = dtout * 3600    
    write(*,*) 'Output time step  :', dtout, ' seconds'      
    
    if (mod(dtout, dt) .NE. 0) then
      write(*,*) 'DTOUT not divisible by DT'
      write(*,*) '**** Exiting LADIM ****'
      stop
    end if

    outper = dtout / dt
    nout = nstep / outper
    print *, "outper = ", outper
    print *, "nout   = ", nout

    call readln(linje)
    read(linje, *) Nvarout
    allocate(outvar(Nvarout))
    do i = 1, Nvarout
      call readln(linje)
      read(linje, *) outvar(i)
    end do

    print *, "SETUP.F90 FERDIG" 
! --- Clean up and finish.
!
    close(SUPFIL)




  end subroutine readsup

! -----------------------------
! ***************************** 
! -----------------------------
  subroutine readln(Line)
! -----------------------------
  !
  ! --------------------------------------
  !  Reads a line from a file, skipping
  !  comments and blank lines.
  !
  !  Comments starts with a character from
  !  COMCHAR and continues to the end of line.
  ! 
  !  Readln reads a line.
  !  If the line contains a comment, the comment is removed.
  !  If thereafter the line is blank, the line is skipped
  !  and the procedure repeated with a new line.
  !
  !  Bj�rn �dlandsvik,
  !  IMR, October 1997
  ! --------------------------------------

  !
  ! --- Arguments ---
  !
    character(len=*), intent(out) :: Line
  !   First non comment line read
  !
  ! --- Local constants
  !
    character(len=*), parameter :: COMCHAR = "*!#"
  !   Comment starting characters
  !
  ! --- Local variables
  !
    integer :: ipos
  !   Start position for comment
  !
  ! --------------------------------
  !
    do
    !
    ! --- Read a line
    !
      read(unit=SUPFIL, fmt="(A)") Line
    !
    ! --- Scan for comments
    !
      ipos = scan(Line, COMCHAR)
    !
    ! --- Remove any comments 
    !
      if (ipos /= 0) then  
        Line = Line(:ipos-1)
      end if
    !
    ! --- Exit loop if result is not blank
    !
      if (len_trim(Line) /= 0) then  
        exit
      end if

    end do

  end subroutine readln
! ------------------------------
! ******************************

end module setup

