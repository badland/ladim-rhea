module outnc
!
! Module for output of particle distributions to netCDF files
!

! Alternative structure of the netCDF file
! Dimension, Particle Index (unlimited)
!            Time (NOUT) ! Må kjenne antall utskriftstidspkt på forhånd


!  use inndata, only : imax, jmax, xp, yp, dx, ylong
  use grid, only : imax, jmax, lon_rho, lat_rho
!  use ncutil, only : NF_CLOBBER, NF_GLOBAL, NF_UNLIMITED,               &
!                     NF_INT, NF_INT2, NF_REAL,                          &
!                     nf_create, nf_close, nf_enddef,                    &
!                     nf_put_att_text, nf_put_att_int, nf_put_att_real,  &
!                     nf_def_dim, nf_def_var,                            &
!                     nf_put_vara_real, nf_put_vara_int,                 &
!                     nf_inq_varid, nf_put_vara_int2,                    &
!                     check_err
  use ncutil

  use particles, only : X, Y, Z, lmax, npart, pidnr, Age, Super, &
                        LMAX, pid_max, farm_id, ant_anlegg

  use setup, only : nout, nvarout, outvar, start_time
  use sample, only :  sample_2d_rho, sample_3d_rho
  use input, only : TEMP, SALT
  use time_module, only : time_type, timestr, vec2time, add_days

  implicit none

  logical, parameter, private :: writeZ = .TRUE.


  integer, private :: ncid


! Ikke sikkert dette er riktig spor
! Output variables
!   Positional
  integer, parameter :: pid     = 0   ! Particle id number
  integer, parameter :: px      = 1   ! X position
  integer, parameter :: py      = 2   ! Y position
  integer, parameter :: pz      = 3   ! Depth
  integer, parameter :: plon    = 4   ! Longitude
  integer, parameter :: plat    = 5   ! Latitude
  integer, parameter :: ps      = 6   ! s-coordinate
!   other physical
  integer, parameter :: ptemp   = 7   ! temperature
  integer, parameter :: psalt   = 8   ! salinity

!   biological, defineres annetsteds
  integer, parameter :: page = 9   ! Particle age degree day
  integer, parameter :: psuper = 10 ! Superinduviduals

! Output variable structure
  
  type pvariable  ! Particle variabel structure
    character(len=16) :: name
    integer :: type
    character(len=64) :: standard_name
    character(len=64) :: long_name
    character(len=32) :: unit
    real :: scale_factor
    real :: add_offset
    ! Lage til en %varid også ???
  end type pvariable

!FV  type(pvariable), dimension(0:8) :: pvar
  type(pvariable), dimension(0:12) :: pvar
!FV  type(pvariable), dimension(0:11) :: pvar
  

  type(time_type) :: creation_time
  
  integer, private :: Time_var, pCount_var
  integer, private :: pStart_var, pnr_var
  integer, private :: pArea_var, pRtime_var, pid_count_var, N_areas_var
  private check_err


  contains

subroutine init_output(ncname)
!
!***PURPOSE 
!  Initiate the output particle NetCDF file


! --- Arguments
  character(len=*), intent(in) :: ncname
     ! Name of NetCDF file

!  include 'netcdf.inc'


! --- Local variables  
  integer :: syear, smnd, sday, shour
     ! Start time
  integer :: tmpdate

  integer :: status
     ! Feilkode fra netCDF
  integer :: Pind_dim, Time_dim, Part_dim
     ! Kode for partikkel-indeks og tidsdimensjoner

  integer, dimension(8) :: Date
  integer, dimension(6) :: datevec

  integer :: v
  type(pvariable) :: var
  integer :: varid  


  pvar(pid) = pvariable('pid', nf90_int, '', 'Particle identifier',      &
                        '', 1, 0)

  ! Range: -27 to +627, with resolution 0.01  
!<jonal>  pvar(px) =  pvariable('px', nf90_int2, 'X grid coordinate',          &
!<jonal>                        'grid units', 0.01, 300 )
!<jonal>  pvar(py) =  pvariable('py', nf90_int2, 'Y grid coordinate',          &
!<jonal>                        'grid units', 0.01, 300 )

! Endrer utskrift til real

  pvar(px) =  pvariable('px', nf90_real,                  &
                        '', 'X grid coordinate',          &
                        'grid units', 1, 0 )
  pvar(py) =  pvariable('py', nf90_real,                  &
                        '', 'Y grid coordinate',          &
                        'grid units', 1, 0 )
  pvar(pz) =  pvariable('pz', nf90_real,                  &
                        'depth', 'particle depth',        &
                        'meter', 1, 0 )

  pvar(plon) =  pvariable('plon', nf90_real,                     &
                          'longitude', 'particle longitude',    &
                          'degrees east', 1, 0 )

  pvar(plat) =  pvariable('plat', nf90_real,                     &
                          'latitude', 'particle latitude',       &
                          'degrees north', 1, 0 )

  pvar(ps) = pvariable('ps', nf90_real,                   &
                       '', 's-coordinate',                &
                       'dimensionless', 1, 0)

  pvar(ptemp) = pvariable('ptemp', nf90_real,               &
                          'sea water temperature',          &
                          'particle temperature',           &
                          'degree Celsius', 1, 0)       

  pvar(psalt) = pvariable('psalt', nf90_real,              &
                          'sea_water_practical_salinity',  &
                          'particle salinity',             &
                          'g/kg', 1, 0)

  pvar(page) = pvariable('page', nf90_real,              &
                         '', 'particle age',             &
                         'degree day', 1, 0)

  pvar(psuper) = pvariable('psuper', nf90_real,                   &
                 '', 'number of individuals in superindividual',  &
                 'dimensionless', 1, 0)

!  
! --- Create netcdf-file
!
  status = nf90_create(ncname, NF90_CLOBBER, ncid)
  call check_err(status)

!
! --- Global attributs
!
  status = nf90_put_att(ncid, NF90_GLOBAL, 'FileType', 'Particle file')
  call date_and_time(values=date)
  datevec(1:3) = date(1:3)
  datevec(4:6) = date(5:7)
  
  call vec2time(creation_time, datevec)
  status = nf90_put_att(ncid, NF90_GLOBAL,                         &
                        'Creation_time', timestr(creation_time))
  status = nf90_put_att(ncid, NF90_GLOBAL, 'Grid_size', (/ imax, jmax /))
!  status = nf90_put_att_text(ncid, NF90_GLOBAL, 'horcoordsys',    &
!                 24, 'polar stereographic grid')
!  status = nf90_put_att_real(ncid, NF90_GLOBAL, 'Grid_def',       &
!                  NF90_REAL, 4, (/ xp, yp, dx, ylong /))
  status = nf90_put_att(ncid, NF90_GLOBAL, 'history',              &
                       'Created by LADIM')
  status = nf90_put_att(ncid, NF90_GLOBAL, 'institution',          &
                       'Institute of Marine Research')
  

!  if (FixDepth) then
!    status = nf90_put_att_real(ncid, NF90_GLOBAL, 'Fixed_depth',  &
!                  NF90_REAL, 1, FixDepthValue)
!  end if

!
! --- Define dimensions
!

!! BAA: 2014-06-27
!! Weakness: dimension names are too similar
!! But changing Particle_Index is not backwards compatible
!! Particle is for static information that does not change
!! with time. Such as: release_area, release_time
!! For continuous sources this should be an umlimited dimension
!! but instead we overkill and use LMAX.

  status = nf90_def_dim(ncid, 'Particle_Index', NF90_UNLIMITED, Pind_dim)
  call check_err(status)
  status = nf90_def_dim(ncid, 'Particle', LMAX, Part_dim)
  call check_err(status)
  status = nf90_def_dim(ncid, 'Time', Nout + 1, Time_dim)
  call check_err(status)


!
! --- Define variables
!

  ! - Time variables

  status = nf90_def_var(ncid, 'Time', NF90_REAL, Time_dim, Time_var)
  call check_err(status)
  status = nf90_put_att(ncid, Time_var,                     &
           'units', 'days since ' // timestr(start_time))
  call check_err(status)

  status = nf90_def_var(ncid, 'pStart',  NF90_INT, Time_dim, pStart_var)
  call check_err(status)
  status = nf90_put_att(ncid, pStart_var, 'long_name',                   &
           'Start index of particle distribution at given time')
  call check_err(status)

  status = nf90_def_var(ncid, 'pCount', NF90_INT, Time_dim, pCount_var)
  call check_err(status)
  status = nf90_put_att(ncid, pCount_var, 'long_name',                   &
           'Number of particles at given time')
  call check_err(status)

  ! - Particle variables

  status = nf90_def_var(ncid, 'Release_area', NF90_INT, Part_dim, pArea_var)
  call check_err(status)
  status = nf90_put_att(ncid, pArea_var, 'long_name',                   &
           'Particle release area code')
  call check_err(status)

  status = nf90_def_var(ncid, 'Release_time', NF90_REAL,       &
                        Part_dim, pRtime_var)
  call check_err(status)
  status = nf90_put_att(ncid, pRtime_var, 'long_name',         &
           'Particle release time')
  call check_err(status)
  status = nf90_put_att(ncid, pRtime_var, 'units',             &
           'days since ' // timestr(start_time))
  call check_err(status)

  !status = nf90_def_var(ncid, 'pid_max', NF90_INT, (/ /), pid_max_var)
  !call check_err(status)
  !status = nf90_put_att(ncid, pid_max_var, 'long_name'          &
  !         'Total number of particles used')
  !call check_err(status)
  
  status = nf90_def_var(ncid, 'N_areas', NF90_INT, varid=N_areas_var)
  call check_err(status)


  status = nf90_def_var(ncid, 'pid_count', NF90_INT, Time_dim, pid_count_var)
  call check_err(status)
  status = nf90_put_att(ncid, pid_count_var, 'long_name',         &
           'Number of particles used so far')
  call check_err(status)

  ! - Particle index variables
  do v = 1, Nvarout

    var = pvar(outvar(v))

    status = nf90_def_var(ncid, var%name, var%type, Pind_dim, varid)
    call check_err(status)

    if (len_trim(var%standard_name) > 0) then
      status = nf90_put_att(ncid, varid, 'standard_name', var%standard_name)
      call check_err(status)
    end if
      
    status = nf90_put_att(ncid, varid, 'long_name', var%long_name)
    call check_err(status)

    status = nf90_put_att(ncid, varid, 'units', var%unit)
    call check_err(status)

    if (var%scale_factor /= 1.0) then
      status = nf90_put_att(ncid, varid, 'scale_factor', var%scale_factor)
      call check_err(status)
    end if 

    if (var%add_offset /= 0.0) then
       status = nf90_put_att(ncid, varid, 'add_offset', var%add_offset)
       call check_err(status)
    end if
  end do


!
! --- Leave definition mode 
!
  status = nf90_enddef(ncid)
  call check_err(status)

!
! --- Print time independent data
!
  status = nf90_put_var(ncid, N_areas_var, ant_anlegg)
  call check_err(status)


  print *, 'INIT_OUTPUT: finished'

end subroutine init_output

!
! -----------------------------------
!

subroutine write_output(tid)
!
!***PURPOSE
!  Write the particle data at one time step to the NetCDF file

  implicit none 

!
! --- Arguments
!
  real, intent(in) :: tid
     ! Time

!  include 'netcdf.inc'


  integer status
!  integer, dimension(2) :: start, count

  integer, save :: outnr = 0
  integer, save :: pStart = 1

  integer :: v
  type(pvariable) :: var
  integer :: varid

  real, dimension(LMAX) :: F ! Field evaluated at particle positions 
  integer :: l
   
  
! --------
   
  outnr = outnr + 1


  print *, ''
  !print *,  'WRITE_OUTPUT: time, outnr, Npart = ', tid, outnr, Npart
  write(*, "(A, X, I4, X, F8.3, X, A)")               &
         'write_output:', outnr, tid, timestr(add_days(start_time, tid))
  print *, '  Number of particles: aktive, totalt ', Npart, pid_max

 
  !
  ! --- Write the time variables
  !
  status = nf90_put_var(ncid, Time_var, tid, (/outnr/))
  call check_err(status)

  status = nf90_put_var(ncid, pCount_var, Npart, (/outnr/))
  call check_err(status)

  status = nf90_put_var(ncid, pStart_var, pStart, (/outnr/))
  call check_err(status)

  status = nf90_put_var(ncid, pid_count_var, pid_max, (/outnr/))
  call check_err(status)

  ! Overwrite
  !status = nf90_put_var(ncid, pid_count_var, pid_count_var)
  !call check_err(status)
  
  ! Partially overwrite (could be improved)
  status = nf90_put_var(ncid, pArea_var, farm_id(1:pid_max), (/1/), (/pid_max/))
  call check_err(status)

  print *, '  Linje 396'

  !
  ! --- Write the particle variables
  !
  do v = 1, Nvarout
    var = pvar(outvar(v))
    select case (outvar(v))
    case(pid)
      call write_int(pidnr)
      print *, 'pid = ', pidnr(1)
    case(px)
      call write_real(X)
!      print *, 'px = ', X(1)
    case(py)
      call write_real(Y)
    case(pz)
      ! print *, 'pz for call = ', Z(1)
      call write_real(Z)
      print *, 'pz = ', Z(1)
    case(plon)
      do l = 1, Npart
        call sample_2d_rho(lon_rho, x(l), y(l), F(l))
      end do
      print *, 'plon = ', F(1)
      call write_real(F)    
    case(plat)
      do l = 1, Npart
        call sample_2d_rho(lat_rho, x(l), y(l), F(l))
      end do
      print *, 'plat = ', F(1)
      call write_real(F)    
    case(ptemp)
      do l = 1, Npart
        call sample_3d_rho(TEMP, x(l), y(l), z(l), F(l))
      end do
      print *, 'ptemp = ', F(1)
      call write_real(F)    
    case(psalt)
      do l = 1, Npart
        call sample_3d_rho(SALT, x(l), y(l), z(l), F(l))
      end do
      print *, 'psalt = ', F(1)
      call write_real(F)    
    case(page)
        F = Age(:) ! age in degree day
      print *, 'page = ', F(1)
      call write_real(F)
    case(psuper)
       F=Super(:)
       call write_real(F)
    end select

  end do

  pStart = pStart + Npart


  ! Flush the data
  status = nf90_sync(ncid)
  call check_err(status)
 
  print *, 'Write_output: Ferdig'

  contains

  ! NF90 generic interface could simplify the below
 
  ! --------------------------------------
  subroutine write_int2(F)
    real, dimension(:), intent(in) :: F

    integer(kind=2), dimension(size(F)) :: F2
    integer :: varid

    F2 = int((F - var%add_offset) / var%scale_factor, kind=2)

    status = nf90_inq_varid(ncid, var%name, varid)
    call check_err(status)
    status = nf90_put_var(ncid, varid, F2(1:Npart), (/pStart/), (/Npart/))
    call check_err(status)

  end subroutine write_int2
  
  ! ---------------------------------
  subroutine write_int(F)
    integer, dimension(:), intent(in) :: F
    status = nf90_inq_varid(ncid, var%name, varid)
    call check_err(status)
    status = nf90_put_var(ncid, varid, F(1:Npart), (/pStart/), (/Npart/))
    call check_err(status)
  end subroutine write_int

  ! ---------------------------------
  subroutine write_real(F)
    real, dimension(:), intent(in) :: F
write(*,*)'write_real ',var%name
    status = nf90_inq_varid(ncid, var%name, varid)
    call check_err(status)
write(*,*)'write_real ',pstart,npart
    status = nf90_put_var(ncid, varid, F(1:Npart), (/pStart/), (/Npart/))
    call check_err(status)
  end subroutine write_real

  
end subroutine write_output

!
! ----------------------------------
!
subroutine close_output()

  integer :: status

  status = nf90_close(ncid)
  call check_err(status)

end subroutine close_output

end module outnc
