module ibm

  use particles,        only : npart,X,Y,Z, randhoronly, Age, Super, LMAX
  use grid,             only : H, lat_rho, imax, jmax!, cop_dens
  use sample,           only : sample_2d_rho, sample_3d_rho
  use input,            only : TEMP,SALT
  use setup,            only : dt
  use time_module
  use surface_light
  use generate_rand_number

  contains
  !
  ! ------------------------------
  !
  subroutine louse_behaviour(latime,tid)
  !
  ! ------------------------------
  !

    real, intent(in) :: latime
    type(time_type), intent(in) :: tid
!    integer, dimension(imax, jmax), intent(in) :: COP_DENS

    real, parameter :: sec2day = 1.0/86400.0
    integer :: l, ii, jj, ind
    integer, dimension(6) :: timevec 
!    type(time_type) :: tid
    real :: mm2m, Eb
    real :: Lat0, k , g, swim_vel, R, P, Rand
    real ::  W
    real, dimension(LMAX), save :: tempB
    logical :: fixdyp = .false., siste
!    integer, dimension(imax,jmax) :: COP_DENS

! latime         : Time in decimal days since initialization
! IER            : Error return from call to getr (get reactive distance)
! mm2m           : Convert from mm to m
! s_light        : surface light
! T0, Lat0       : dummy temp and lat interpolated to position of particle
! timevec        : Time vector - y m d h min s
!
! cop_dens(imax,jmax) : Akkumulert tetthet av infektive copepoditter
! siste = true  for siste kall av rutina (og cop_dens skal skrives til fil)
! fixdyp = om z-skal variere eller ikke

    mm2m = 0.001
    g = 9.81
    tempB = 7.0 ! setter default temperatur


    R = 0.17/(24*3600.0)
    k = 0.2
!   absolute velocities and displacements (EXPERIMENT WITH THEESE!)
    swim_vel = 5E-4     ! 0.0005 m/s
!    light_vel = 5E-4    ! 0.0005 m/s
!    D = 1E-3 ! Sjekk i forhold til tidssteg i trackpart.f90
!     D = 1E-5   ! NorKyst
     D = 1E-5   ! NorKyst Anne
!    D = 1E-8    <-1E-5,1E-5>
!    D = 2E-5    <-5E-4,5E-4>
!    D = 8E-8    <-3E-5,3E-5>

    call time2vec(timevec, tid)

    do l = 1, npart 
!    print *, 'npart(l) = ', npart
!       print *, 'x = ', X(l)

! Finner omgivelsestemp og døgngrader
    call sample_3d_rho(TEMP, X(l), Y(l), Z(l), T0)

! tester om temperatur er innenfor rimelige grenser
    if( T0 > 40) then 
      T0 = tempB(l)
    else
      tempB(l) = T0
    endif

    Age(l)= Age(l) + (T0*sec2day)*dt

    P = exp(-(R * dt))

    Super(l)=Super(l)*P

!    do ind = 1,Super(l)
!       call random_number(Rand)
!       if (Rand .gt. P) then
!          Super(l) = Super(l)-1
!       else
!          Super(l) = Super(l)
!       endif
!    enddo

!**********************SALMON LOUSE BEHAVIOUR********************
! Finner lys ved gitt djup
call sample_2d_rho(lat_rho, x(l), y(l), Lat0)
call surf_light(timevec,Lat0,s_light)

Eb = s_light*exp(k*Z(l))
if (Age(l) < 50.0) then
   if (Eb < 0.392) then
      W = 0.0
   else
      W = swim_vel
   endif
else
    if (Eb<2.06E-5) then
       W = 0.0
    else
       W = swim_vel
    endif
endif

! Finner omgivelsessalt og angir vertikal hast dersom salt<20 psu
call sample_3d_rho(SALT, X(l), Y(l), Z(l), S0)
if (S0 < 20.0) then
   W = -swim_vel
endif

if (.not. fixdyp) then
    call gran(random)
    Z(l) = Z(l) + (W + random*sqrt(2*D/dt)) * dt
endif

if (Z(l) .gt. 0.0) then
   Z(l) = - Z(l)
endif

! Do not allow particles below 20 m
if (Z(l) .lt. -20.0) then
   Z(l) = - 20.0
endif 

!**********************SALMON LOUSE BEHAVIOUR********************  
    end do


  end subroutine louse_behaviour

end module ibm
