module input
!
! Lars: tar vekk lesing av AKt og AKv som ikke brukes i lakselus
!

!  use ncutil, only : nf_open, nf_close, NF_NOWRITE, check_err,    &
!                     getatt, getvar, getvartime, dimlen
  use ncutil
  use time_module
  use grid, only : imax, jmax, kmax,                              &
                   mask_rho, mask_u, mask_v

  use setup 
!  use particles,        only : X,Y,Z

  implicit none

  integer, dimension(:), allocatable :: ocean_step
  integer :: ocean_l

  real, dimension(:,:,:), allocatable :: U
  real, dimension(:,:,:), allocatable :: V
  real, dimension(:,:,:), allocatable :: TEMP
  real, dimension(:,:,:), allocatable :: SALT
  real, dimension(:,:,:), allocatable :: AKt
  real, dimension(:,:,:), allocatable :: AKs

  real, dimension(:,:,:), allocatable :: DU
  real, dimension(:,:,:), allocatable :: DV
  real, dimension(:,:,:), allocatable :: DTEMP
  real, dimension(:,:,:), allocatable :: DSALT
  real, dimension(:,:,:), allocatable :: DAKt
  real, dimension(:,:,:), allocatable :: DAKs

  real, dimension(:,:,:), allocatable, private :: Unew
  real, dimension(:,:,:), allocatable, private :: Vnew
  real, dimension(:,:,:), allocatable, private :: TEMPnew
  real, dimension(:,:,:), allocatable, private :: SALTnew
  real, dimension(:,:,:), allocatable, private :: AKtnew
  real, dimension(:,:,:), allocatable, private :: AKsnew

  integer :: input_step

  character(len=80), private :: froot  ! File name root
  integer, private  :: filenum    ! Input file number
  integer, private  :: filepos    ! Time step in input file
  integer, private  :: ntimes     ! Number of time steps in input file
  integer, private  :: ncid       ! NetCDF file identifier
  integer, private  :: ncid_avg   !avgfile for AKt and AKs
  character(len=10), private :: time_units
  type(time_type), private   :: time_origin
  type(time_type), private   :: next_time
  integer, private :: input_step0

  contains

  !
  ! *****************************************************
  ! 
  subroutine init_input

  ! -----
  ! Initializing routine for the input from the ocean model
  !
  ! H�ndterer n� bare serie-filer
  !  lage til en svitsj (f.eks. i setup-rutinen)
  !  slik at kan og kj�re enkelt-filer

    real(kind=8) :: dtime
    type(time_type) :: start_ocean
    type(time_type) :: ocean_time
 !   integer :: i
    integer :: fnumpos   ! position of file number in file name
    character(len=80) :: filename
 !   integer :: step0
    logical :: found_file
    integer :: nfile
    integer :: l   ! time index
    integer :: oldfilepos
    integer :: oldfilenum

    ! Allocate arrays
    allocate(U(1:imax+1, 0:jmax+1, 1:kmax))
    allocate(V(0:imax+1, 1:jmax+1, 1:kmax))
    allocate(TEMP(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(SALT(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(AKt(0:imax+1,  0:jmax+1, 1:kmax+1))
    allocate(AKs(0:imax+1,  0:jmax+1, 1:kmax+1))

    allocate(DU(1:imax+1, 0:jmax+1, 1:kmax))
    allocate(DV(0:imax+1, 1:jmax+1, 1:kmax))
    allocate(DTEMP(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(DSALT(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(DAKt(0:imax+1,  0:jmax+1, 1:kmax+1))
    allocate(DAKs(0:imax+1,  0:jmax+1, 1:kmax+1))

    allocate(Unew(1:imax+1, 0:jmax+1, 1:kmax))
    allocate(Vnew(0:imax+1, 1:jmax+1, 1:kmax))
    allocate(TEMPnew(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(SALTnew(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(AKtnew(0:imax+1,  0:jmax+1, 1:kmax+1))
    allocate(AKsnew(0:imax+1,  0:jmax+1, 1:kmax+1))


    ! TEST: SERIE FILER ???
! Lars    fnumpos = index(fname ,'_0000.nc')
    fnumpos = index(fname ,'_0001.nc')
!    fnumpos = index(fname ,'_0000.nc')
!write(*,*)'fnumpos',fnumpos
 
    if ( fnumpos> 0) then
      filenum = start_fnum !Lars start_fnum er ikke deklarert
      froot = fname(1:fnumpos)  ! file-navn med xxxx_
    else
      filenum = -1 
      filename = fname
    end if
    found_file = .false.
    nfile = filenum

! Lars 
! foerste his-fil starter paa 0002
!
!write(*,*)'nfile ',nfile, filename
!pause
!Lars: oeker nfile med 2
nfile = nfile + 1

    ! --- LOOP OVER INPUT FILES ---
!FV found_file er false inntil filen er aapnet. .not. found_file er 
!FV derfor lik true
    do while (.not. found_file)
      ! Open the file

!write(*,*) froot, nfile, '.nc'
!pause
      write(filename, '(A,I4.4,A3)') trim(froot), nfile, '.nc'
!FV      write(filename, '(A,I3.3,A3)') trim(froot), nfile, '.nc'

!write(*,*)'nfile ',nfile, filename
!pause

      call open_input(filename)
      ! --- loop over time steps within the file
    print *, 'ntimes', ntimes
      do l = 1, ntimes
        call get_time(l, ocean_time)        
        if (ocean_time > start_time) then 
          if (l > 1) then
            oldfilepos = l - 1
            oldfilenum = nfile
          else
            ! Bactrack a file
!            oldfilepos = -1  ! last position BUG
            oldfilepos = ntimes  ! last position
            oldfilenum = nfile - 1
          end if
          found_file = .true.
          next_time = ocean_time
          filepos = l
          filenum = nfile
          exit
        end if
      end do ! l

      call close_input()
      nfile = nfile+1
    end do ! while

    ! Read last field <= start_time
    
    write(filename, '(A,I4.4,A3)') trim(froot), oldfilenum, '.nc'
!FV    write(filename, '(A,I3.3,A3)') trim(froot), oldfilenum, '.nc'
    call open_input(filename)
    call get_time(oldfilepos, ocean_time)
    ! Beregn tidsteg:
    ! 0 = start, 
    input_step0 = nint( (ocean_time-start_time) * 86400.0 / dt)
    input_step = nint( (next_time-start_time) * 86400.0 / dt)

    print *, "siste tidspunkt <= start_time"
    print *, "input_step0 = ", input_step0
    print *, "next_time   = ", input_step

    ! Read the current at last time <= start_time
    call read_current(oldfilepos, Unew, Vnew)
    call read_temp(oldfilepos, TEMPnew)
    call read_salt(oldfilepos, SALTnew)
!Lars    call read_AKt(oldfilepos, AKtnew)
!Lars    call read_AKs(oldfilepos, AKsnew)

    ! Call first ordinary read
    call get_input()
    
    ! Interpolate to start_time 
    ! Remember that input_step0 <= 0
    U = U - input_step0*DU
    V = V - input_step0*DV
    TEMP = TEMP - input_step0*DTEMP
    SALT = SALT - input_step0*DSALT
 !Lars   AKt  = AKt  - input_step0*DAKt
 !Lars   AKs  = AKs  - input_step0*DAKs


    print *, "init_input: Finished"


  end subroutine init_input

  !
  ! -------------------------------------------
  !
  subroutine get_input()


!    integer, intent(out) :: itime

    integer :: k
    character(len=100) :: filename

    ! Update the velocity fields
    U = Unew
    V = Vnew
    TEMP = TEMPnew
!    print *, 'SALT(X(1),Y(1),:) = ', SALT(X(1),Y(1),:)
    SALT = SALTnew
!    AKt  = AKtnew
!    AKs  = AKsnew

    print *, "get_input: filepos = ", filepos

    ! Read next current field
    call read_current(filepos, Unew, Vnew)
    call read_temp(filepos, TEMPnew)
    call read_salt(filepos, SALTnew)
 !   call read_AKt(filepos, AKtnew)
 !   call read_AKs(filepos, AKsnew)


    
    ! Compute increments
    DU = (Unew - U) / (input_step - input_step0)
    DV = (Vnew - V) / (input_step - input_step0)
    DTEMP = (TEMPnew - TEMP) / (input_step - input_step0)
    DSALT = (SALTnew - SALT) / (input_step - input_step0)
 !   DAKt = (AKtnew - AKt) / (input_step - input_step0)
 !   DAKs = (AKsnew - AKs) / (input_step - input_step0)

    ! Finn neste tidspunkt: next_time
    filepos = filepos + 1
    print *, "---- filepos, ntimes = ", filepos, ntimes
    if (filepos > ntimes) then
      ! Continue with next input file
      call close_input()
      filenum = filenum + 1
      filepos = 1
      write(filename, '(A,I4.4,A3)') trim(froot), filenum, '.nc'
!FV      write(filename, '(A,I3.3,A3)') trim(froot), filenum, '.nc'
      call open_input(filename)
    end if
    call get_time(filepos, next_time)
    input_step0 = input_step
    input_step = nint( (next_time-start_time) * 86400.0 / dt)

  end subroutine get_input

  !
  !
  ! --------------------------------------------
  !
  subroutine open_input(fname)

    character(len=*), intent(in) :: fname

    character(len=80) :: tstring
    integer :: status
    integer :: status_avg

    ! Open the NetCDF file
!Lars    status_avg = nf90_open("/work/anneds/lars/roms_3_1k_904/Output_exp5_200m_WRF/avg.nc", NF90_NOWRITE, ncid_avg)

    ! Error handling
 !   if (status_avg /= 0) then
 !     write(*,*)  " "
 !     write(*,*) "open_input: can not open NetCDF file: /work/anneds/lars/roms_3_1k_904/Output_exp5_200m_WRF/avg.nc"
 !     call check_err(status)
 !   end if

    ! Open the NetCDF file
    status = nf90_open(fname, NF90_NOWRITE, ncid)

    ! Error handling
    if (status /= 0) then
      write(*,*)  " "
      write(*,*) "open_input: can not open NetCDF file: ", fname
      call check_err(status)
    end if


    write(*,*) "opened file: ", fname
    ! Get ntimes, time_units, and time_origin
    ntimes = dimlen(ncid, 'ocean_time')
    call getatt(ncid, "ocean_time", "units", tstring)
    if (tstring(1:1) == "d") then
      time_units = "days"
      tstring = tstring(12:)
    else if (tstring(1:1) == "h") then
      time_units = "hours"
      tstring = tstring(13:)
    else if (tstring(1:1) == "s") then
      time_units = "seconds"
      tstring = tstring(15:)
    else
      print *, "***Illegal units attribute: ", tstring
      stop
    end if
    time_origin = tstring
  
  end subroutine open_input





  !
  ! -----------------------------------
  !
  subroutine update_input()

    u = u + du
    v = v + dv
    TEMP = TEMP + dTEMP
    SALT = SALT + dSALT
 !Lars   AKt  = AKt  + dAKt
 !Lars   AKs  = AKs  + dAKs

  end subroutine update_input

  !
  !
  !
  subroutine read_current(l,U,V)

    integer, intent(in) :: l
    real, dimension(:,:,:), intent(out) :: U
    real, dimension(:,:,:), intent(out) :: V
    integer :: k
    call getvartime(ncid, "u", l, U)
    call getvartime(ncid, "v", l, V)
    ! Make sure currents are zero on land
    ! Dette er gjort i ROMS (men kanskje ikke for initalf-feltene)
    do k = 1, kmax
      U(:,:,k) = mask_u   * U(:,:,k)
      V(:,:,k) = mask_v   * V(:,:,k)
    end do
  end subroutine read_current
 
  subroutine read_temp(l,TEMP)
    integer, intent(in) :: l
     real, dimension(:,:,:), intent(out) :: TEMP
    call getvartime(ncid, "temp", l, TEMP)
  end subroutine read_temp
 
  subroutine read_salt(l,SALT)
    integer, intent(in) :: l
     real, dimension(:,:,:), intent(out) :: SALT
    call getvartime(ncid, "salt", l, SALT)
  end subroutine read_salt

  subroutine read_AKt(l,AKt)
    integer, intent(in) :: l
     real, dimension(:,:,:), intent(out) :: AKt
    call getvartime(ncid_avg, "AKt", l, AKt)
  end subroutine read_Akt
 
  subroutine read_AKs(l,AKs)
    integer, intent(in) :: l
     real, dimension(:,:,:), intent(out) :: AKs
    call getvartime(ncid_avg, "AKs", l, AKs)
  end subroutine read_Aks

  ! ------------------------------------------------
  !
  subroutine get_time(filepos, ocean_time)
    ! Return the time stamp of timestep=filepos in 
    ! a open input file

    integer, intent(in) :: filepos
    type(time_type), intent(out) :: ocean_time
     
    real(kind=8) :: otime
!FV     write(*,*)'keller getvartime',ncid,filepos,otime
    call getvartime(ncid, "ocean_time", filepos, otime)
    if (time_units == "seconds") then
      otime = otime / (24*3600)
    else if (time_units == "hours") then
       otime = otime / 24
    end if
    ocean_time = time_origin + otime
!FV    stop
  end subroutine get_time
  ! 
  ! ----------------------------------
  !
  subroutine close_input

    integer :: status
    integer :: status_avg

    status = nf90_close(ncid)
!Lars    status_avg = nf90_close(ncid_avg)
    call check_err(status)
!Lars    call check_err(status_avg)

  end subroutine close_input

  !
  ! -----------------------------------
  !

end module input




