module trackpart

! Bruk setup, only => f�r advarsel, size change
!  use setup, only : dt
  use setup
  use grid,      only : pm, pn, imax, jmax, mask_rho, hc, H
  use particles, only : x, y, z, npart, xp, yp, zp, up, vp
  use input,     only : U, V,              &
                        DU, DV, temp               
  use sample,    only : sample_UV, sample_2d_rho, sample_UV2, sample_3d_rho
  use generate_rand_number
  implicit none

  integer :: n   = 1
  integer :: nm1 = 2
  integer :: nm2 = 3
  integer :: nm3 = 4



  contains


  subroutine movepart2

    ! Runge-Kutta 4th order
    ! Ogs� ha en Runge-Kutta 2 dre orden ?
    
    real, parameter :: D = 0.02 !0.02   !Lars0.02  !0.1  !1.0  !50.0

    integer :: l  ! Particle index
    real :: x0, y0, z0
    real :: x1, y1, z1
    real :: x2, y2, z2
    real :: x3, y3, z3
    real :: u1, v1
    real :: u2, v2
    real :: u3, v3
    real :: u4, v4
    real :: uadv, vadv
    real :: xadv, yadv
    real :: pm0, pn0
    real :: random
    !integer, dimension(1) :: k
    real :: tval
    integer :: i, j

    !print *, "movepart2: start"


    do l = 1, Npart
!FV maa sjekke at partiklene ligger inne i modellomr.
    if ((0 < x(l)).and.(x(l) < imax).and.(0 < y(l)).and.(y(l) < jmax)) then

      x0 = x(l)
      y0 = y(l)
      z0 = z(l)

      call sample_2d_rho(pm, x0, y0, pm0)
      call sample_2d_rho(pn, x0, y0, pn0)    
      call sample_UV2(U, V, DU, DV, x0, y0, z0, u1, v1, 0.0)
      x1 = x0 + 0.5*u1*dt*pm0
      y1 = y0 + 0.5*v1*dt*pn0
      z1 = z0

      call sample_UV2(U, V, DU, DV, x1, y1, z1, u2, v2, 0.5)
      x2 = x0 + 0.5*u2*dt*pm0
      y2 = y0 + 0.5*v2*dt*pn0
      z2 = z0

      call sample_UV2(U, V, DU, DV, x2, y2, z2, u3, v3, 0.5)
      ! Her var en bug, OK n� ?
      !x3 = x0 + 0.5*u2*dt*pm0
      !y3 = y0 + 0.5*v2*dt*pn0
      x3 = x0 + u2*dt*pm0
      y3 = y0 + v2*dt*pn0
      z3 = z0

      call sample_UV2(U, V, DU, DV, x3, y3, z3, u4, v4, 1.0)
     
      uadv = (u1 + 2*u2 + 2*u3 + u4) / 6.0
      vadv = (v1 + 2*v2 + 2*v3 + v4) / 6.0


      call gran(random)
      xadv = (uadv + random*sqrt(2*D/dt)) * pm0 * dt
      call gran(random)
      yadv = (vadv + random*sqrt(2*D/dt)) * pn0 * dt



      x(l) = x0 + xadv
      y(l) = y0 + yadv


      !if (l == 1) then
      !  print *, "x, uadv, xadv = ", x(l), uadv, xadv
      !  print *, "y, vadv, yadv = ", y(l), vadv, yadv
      !end if ! (l == 




! sjekker at partikkelen ikke havner p� land
! har da temp udefinert, lar partikkelen i ro
      ! Grid cell containing x, y
      i = nint(x(l))
      j = nint(y(l))
      if (z(l) < -H(i,j)) then
         x(l) = x0
         y(l) = y0
      end if
    
  
       



      
    end if   

    
    end do

    !print *, "movepart2: finished"

  end subroutine movepart2

end module trackpart




