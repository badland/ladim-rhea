module trackpart

! Bruk setup, only => f�r advarsel, size change
!  use setup, only : dt
  use setup
  use grid,      only : pm, pn, imax, jmax, mask_rho, mask_u, mask_v
  use particles, only : x, y, z, npart, xp, yp, zp, up, vp, randhoronly
  use input,     only : U, V, DU, DV, SALT, TEMP               
  use sample,    only : sample_UV, sample_2d_rho, sample_UV2, sample_3d_rho
  use generate_rand_number
  implicit none

  integer :: n   = 1
  integer :: nm1 = 2
  integer :: nm2 = 3
  integer :: nm3 = 4


  contains

  subroutine movepart

  ! Enkel Euler-Forward (for bare aa ha noe i det hele tatt)

    integer :: l  ! Particle index
    real :: x0, y0, z0
    real :: uadv, vadv
    real :: xadv, yadv
    real :: pm0, pn0

    do l = 1, Npart
!FV maa sjekke at partiklene ligger inne i modellomr.
    if ((0 < x(l)).and.(x(l) < imax).and.(0 < y(l)).and.(y(l) < jmax)) then

      x0 = x(l)
      y0 = y(l)
      z0 = z(l)
      call sample_UV(U, V, x0, y0, z0, uadv, vadv)

      ! Gjre enklere ?, bare ta pm, pn fra gridcellen
      ! som inneholder (x,y)
      ! OK dersom pm, pn endres langsomt (som de br gjre)
      call sample_2d_rho(pm, x0, y0, pm0)
      call sample_2d_rho(pn, x0, y0, pn0)

      xadv = uadv * pm0 * dt
      yadv = vadv * pn0 * dt

      x(l) = x0 + xadv
      y(l) = y0 + yadv
      else
       x(l) = x(l)
       y(l) = y(l)
      
      end if   

    end do

  end subroutine movepart

  subroutine movepart2

    ! Runge-Kutta 4th order
    ! Ogs� ha en Runge-Kutta 2 dre orden ?
    

    integer :: l  ! Particle index
    real :: x0, y0, z0
    real :: x1, y1, z1
    real :: x2, y2, z2
    real :: x3, y3, z3
    real :: u1, v1
    real :: u2, v2
    real :: u3, v3
    real :: u4, v4
    real :: uadv, vadv
    real :: xadv, yadv
    real :: pm0, pn0, S0, T0
    real :: random

! dt=3600 s and D=50   m2/s -> random walk step is limited by (-0.167,0.167) m/s
! dt= 900 s and D=50   m2/s -> random walk step is limited by (-0.333,0.333) m/s
! dt=3600 s and D=1    m2/s -> random walk step is limited by (-0.024,0.024) m/s
! dt= 900 s and D=1    m2/s -> random walk step is limited by (-0.047,0.047) m/s
! dt= 900 s and D=0.25 m2/s -> random walk step is limited by (-0.024,0.024) m/s
! dt= 900 s and D=0.1  m2/s -> random walk step is limited by (-0.015,0.015) m/s
! dt= 180 s and D=0.02 m2/s -> random walk step is limited by (-0.015,0.015) m/s

    real, parameter :: D = 0.02 !0.02   !Lars0.02  !0.1  !1.0  !50.0

!    call sample_3d_rho(SALT, X(l), Y(l), Z(l), S0)

 !   print *, 'Using Runga-Kutta 4th order'
    do l = 1, Npart
!FV maa sjekke at partiklene ligger inne i modellomr.
!    if (S0 == 0) then
!      x(l) = x(l)
!      y(l) = y(l)
!jonal    if ((0 < x(l)).and.(x(l) < imax).and.(0 < y(l)).and.(y(l) < jmax)) then

      x0 = x(l)
      y0 = y(l)
      z0 = z(l)

    IF (x0 > 0.5 .AND. x0 < REAL(imax)+0.5 .AND. y0 > 0.5 .AND. y0 < REAL(jmax)+0.5) THEN

      call sample_2d_rho(pm, x0, y0, pm0)
      call sample_2d_rho(pn, x0, y0, pn0)

!      print *, "1st execution of sample_UV2"
      call sample_UV2(U, V, DU, DV, x0, y0, z0, u1, v1, 0.0)
      x1 = x0 + 0.5*u1*dt*pm0
      y1 = y0 + 0.5*v1*dt*pn0
      z1 = z0

      IF (x1 > 0.5 .AND. x1 < REAL(imax)+0.5 .AND. y1 > 0.5 .AND. y1 < REAL(jmax)+0.5) THEN
!      print *, "2nd execution of sample_UV2"
      call sample_UV2(U, V, DU, DV, x1, y1, z1, u2, v2, 0.5)
      x2 = x0 + 0.5*u2*dt*pm0
      y2 = y0 + 0.5*v2*dt*pn0
      z2 = z0

      IF (x2 > 0.5 .AND. x2 < REAL(imax)+0.5 .AND. y2 > 0.5 .AND. y2 < REAL(jmax)+0.5) THEN
!      print *, "3rd execution of sample_UV2"
      call sample_UV2(U, V, DU, DV, x2, y2, z2, u3, v3, 0.5)
      ! Her var en bug, OK n� ?
      !x3 = x0 + 0.5*u2*dt*pm0
      !y3 = y0 + 0.5*v2*dt*pn0
      x3 = x0 + u2*dt*pm0
      y3 = y0 + v2*dt*pn0
      z3 = z0

      IF (x3 > 0.5 .AND. x3 < REAL(imax)+0.5 .AND. y3 > 0.5 .AND. y3 < REAL(jmax)+0.5) THEN
!      print *, "4th execution of sample_UV2"
      call sample_UV2(U, V, DU, DV, x3, y3, z3, u4, v4, 1.0)

      uadv = (u1 + 2*u2 + 2*u3 + u4) / 6.0
      vadv = (v1 + 2*v2 + 2*v3 + v4) / 6.0

      IF (randhoronly) THEN
        call gran(random)
        xadv = (uadv + random*sqrt(2*D/dt)) * pm0 * dt
        call gran(random)
        yadv = (vadv + random*sqrt(2*D/dt)) * pn0 * dt
      ELSE
        xadv = uadv * pm0 * dt
        yadv = vadv * pn0 * dt
      ENDIF

!Sjekker om partiklene advekteres inn p� land. I s�fall flyttes de ikke.
     if (mask_rho(int(x0 + xadv),int(y0 + yadv)) == 1) then
      x(l) = x0 + xadv
      y(l) = y0 + yadv 
     else
       x(l) = x0
       y(l) = y0
     endif

!write(*,*)'Ny-gml pos ',x0,x(l),y0,y(l)
!pause

      ELSE
       x(l) = x0
       y(l) = y0
      ENDIF  ! x3

      ELSE
       x(l) = x0
       y(l) = y0
      ENDIF  ! x2

      ELSE
       x(l) = x0
       y(l) = y0
      ENDIF  ! x1

      else
       x(l) = x(l)
       y(l) = y(l)
      
      end if   

      
    end do

  end subroutine movepart2

  !
  ! --------------------------------------------
  !

  subroutine movepart3

    ! 4th order Milne predictor
    ! 4th order Hamming corrector
    ! Algorithm as in ROMS
    

    integer :: l  ! Particle index
    real :: x0, y0, z0
    real :: x1, y1, z1
!    real :: x2, y2, z2
!    real :: x3, y3, z3
    real :: u1, v1
!    real :: u2, v2
!    real :: u3, v3
!    real :: u4, v4
!    real :: uadv, vadv
!    real :: xadv, yadv
    real :: pm0, pn0
!    integer :: n, nm1, nm2, nm3
    real :: w1, w2, w3, w4


    do l = 1, Npart

      x0 = x(l)
      y0 = y(l)
      z0 = z(l)

      call sample_2d_rho(pm, x0, y0, pm0)
      call sample_2d_rho(pn, x0, y0, pn0)

! Algoritme
!
! -- Milne

     ! Beregne u(n) n�

     call sample_UV(U, V, x0, y0, z0, up(l,n), vp(l,n))

     w1=8.0/3.0
     w2=4.0/3.0
!      x* = x(n-3) + dt*(w1*u(n) - w2*u(n-1) + w1*u(n-2))
!     print *, n, nm1, nm2, nm3
!     print *, up(l, n), up(l,nm1), up(l,nm2), up(l,nm3) 
     
     x1 = xp(l,nm3) + dt*pm0*(w1*up(l,n) - w2*up(l,nm1) + w1*up(l,nm2))
     y1 = yp(l,nm3) + dt*pn0*(w1*vp(l,n) - w2*vp(l,nm1) + w1*vp(l,nm2))
     z1 = z0
     

!
! -- Hamming
     w1=9.0/8.0
     w2=1.0/8.0
     w3=3.0/8.0
     w4=6.0/8.0
!     x(n+1) = w1*x(n) - w2*x(n-2)
!              + dt*(w3*u(x*) - w4*u(n) + w3*u(n-1))
!
     call sample_2d_rho(pm, x1, y1, pm0)
     call sample_2d_rho(pn, x1, y1, pn0)
     call sample_UV2(U, V, DU, DV, x1, y1, z1, u1, v1, 1.0)
 
     x(l) = w1*x1 - w2*xp(l,nm2) +                           &
                dt*pm0*(w3*u1 - w4*up(l,n) + w3*up(l,nm1))
     y(l) = w1*y1 - w2*yp(l,nm2) +                           &
                dt*pn0*(w3*v1 - w4*vp(l,n) + w3*vp(l,nm1))

!     x(l) = x1
!     y(l) = y1
     

    end do

! --- M� verifisere at dette funker skikkelig
! M� skyfle indeksene n, nm1, nm2, nm3

    xp(:,n) = x
    yp(:,n) = y



    n   = n   - 1; if (n   < 1) n   = 4    
    nm1 = nm1 - 1; if (nm1 < 1) nm1 = 4    
    nm2 = nm2 - 1; if (nm2 < 1) nm2 = 4    
    nm3 = nm3 - 1; if (nm3 < 1) nm3 = 4    

! Get the values into the arrays
! Kan bli un�ding dersom alt baseres p� disse arrayene
 
    


  end subroutine movepart3
 






end module trackpart




