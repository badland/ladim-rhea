module particles

! Lars - 29mar11: Legger inn timesverdier av utslipp i faste punkt. Dropper ini-fil.
!
!
  use setup, only : dt, readln, start_time
  ! de to under med Milne Hamming
  use input, only : U, V
  use sample, only : sample_UV
  use time_module
  use generate_rand_number
 
  implicit none

 
 integer, parameter :: LMAX = 5000000
!  integer, parameter :: LMAX = 80000
   ! maximum allowed number of particles.
   ! (trengs denne ??, kan ikke dette v�re allokerbart,
   ! problem n�r skal allokeres, kan komme flere partikler seinere
   ! Alternativ: Lese lmax fra sup-fil

  integer :: Npart!
    ! Number of active particles
  integer :: pid_max
    ! Highest particle number used

  integer, dimension(LMAX) ::  pidnr
  real, dimension(LMAX) :: Super
    ! Particle number.
  real, dimension(LMAX) :: X,Y,Z
    ! Position components of the particles



  integer, parameter, private :: piunit = 32
  
  integer :: inihour ! Time (in hours) for next particle release
!  integer :: nnew    ! Number of particle positions 
  integer :: mult    ! Number of particles released in each position
  real :: pi        ! = 2*acos(-1.0) = 3.14159...
  real :: DepthIni  ! Depth for particle release
  real :: RANDOM    ! Bruker random numbers to initialize genes 
  integer :: release_step ! Number of time steps to next release
                          ! -1 = No more releases

  ! For Milne-Hamming
  real, dimension(LMAX,4) :: Xp, Yp, Zp, Up, Vp
!FV
   real, dimension(LMAX) :: Age ! Age of larvae in degdays

   integer, dimension(LMAX) :: area_code

   integer :: k
   real :: RANDbuoy
   logical, parameter :: randhoronly = .TRUE.  !.FALSE.  ! If true, then random walk in the horizontal and no vertical motion, otherwise allow vertical (random) walk

   integer :: i, dt_dager

   integer, allocatable, dimension(:) ::Xpos, Ypos, Zpos, mult_ant
   integer, dimension(LMAX) :: farm_id
   integer, allocatable, dimension(:) :: farm_id_list  
   integer :: ant_anlegg
! public init_particles, read_next_particles

  contains
   
  !
  ! --------------------------------------
  ! 
  subroutine init_particles(partfilename, file_id)

    character(len=*), intent(in) :: partfilename
    integer, intent(in) :: file_id


    print *, 'init_particles: startet'

    open(file_id,file=partfilename)
    read(file_id,*) ant_anlegg

    allocate(Xpos(ant_anlegg),Ypos(ant_anlegg),          &
             Zpos(ant_anlegg),mult_ant(ant_anlegg),      &
             farm_id_list(ant_anlegg))


    do i=1,ant_anlegg
       read(file_id,*) farm_id_list(i), Xpos(i),Ypos(i),Zpos(i),mult_ant(i)
    enddo
    read(file_id,*) dt_dager

    do i=1,LMAX
    Age(i) = 0
    end do

!release_step = -1

  !
  ! --- initiate 
  ! 
    Npart = 0
    pid_max  = 0
    print *, "init_particles finished"
  end subroutine init_particles

  subroutine read_next_particles(file_id)

    integer, intent(in) :: file_id
    
    integer :: dummy

    print *, 'read_next_particles: startet'

    do i=1,ant_anlegg
       read(file_id,*)dummy, Xpos(i),Ypos(i),Zpos(i),mult_ant(i)
    enddo
    read(file_id,*) dt_dager

  end subroutine read_next_particles

  !
  ! --------------------------------------
  !
  subroutine release_particles(ant_anl,Xpos,Ypos,Zpos,MULT_anl)
  ! 
    integer :: l
    real :: X0, Y0
    real :: Z0

    real :: u0, v0
 
    character(len=80) :: line

    integer :: i, j
    integer :: ios

!LarsFEB12
integer :: ant_anl,n
integer, dimension(ant_anl) :: Xpos,Ypos,Zpos,MULT_anl
!LarsFEB12

    integer :: m


    print *, "release_particles: starter"

!LarsFEB12
do n=1,ant_anl
mult = 5
! MULT_anl(n)

      Pidnr(Npart+1:Npart+mult) = (/ (i, i =  pid_max+1, pid_max+mult) /)
      X(Npart+1:Npart+mult) = Xpos(n)  !X0
      Y(Npart+1:Npart+mult) = Ypos(n)  !Y0
      Z(Npart+1:Npart+mult) = Zpos(n) !-abs(Z0)  ! Make sure it is negative
      Super(Npart+1:Npart+mult) = MULT_anl(n)/real(mult)
      Npart = Npart + mult
      do m = 1, mult
        farm_id(pid_max+m) = farm_id_list(n)
      end do ! m
      pid_max  = pid_max  + mult

enddo
!LarsFEB12

    ! Make sure 
    write(*,*) 'release_particles: Npart, Pid_max = ',  Npart, pid_max

  end subroutine release_particles

  !
  ! ------------------------------------------------------
  ! 

  subroutine read_line(file_unit, line, ios)
  ! Return a line from an open file with unit=file_unit
  ! ignoring blank lines and comment lines (starting with "!")
  ! 
    integer, intent(in) :: file_unit
    character(len=*), intent(out) :: line
!    integer, optional, intent(out) :: ios    ! Funket ikke
    integer, intent(out) :: ios  

    do
      read(unit=file_unit, fmt='(A)', iostat=ios) line
      if (ios /= 0) exit               ! Exit at end of file
      if (len_trim(line) == 0) cycle   ! Skip blanks
      if (line(1:1) /= '!') exit       ! Exit on non-comment
    end do
  
  end subroutine read_line
  
  !
  ! ---------------------------------------------------
  !
  subroutine get_time(time_line, stepnr)
  
  ! Read time line from the particle release file
  ! and extract the timestep for the particle release  

    character(len=*) :: time_line     ! The time line as a string
    integer, intent(out) :: stepnr    ! The nearest time step

    type(time_type) :: release_time   ! Absolute release time
    integer :: dtime                  ! Relative release time
    character(len=10) :: dtime_unit   ! Unit of relative time
    
    if (time_line(1:2) == 'TA') then    ! Absolute time
      release_time = time_line(4:)
      stepnr = nint((release_time - start_time)*86400/dt)
      if (stepnr < 0) then
        write(*,*) "ERROR: particle release before start_time"
        stop
      end if
      ! Gi advarsel dersom release_time etter slutt simulering
      ! (brukes for relativ ogs�)

    elseif (time_line(1:2) == 'TR') then  ! Relative time
      read(time_line(4:), *) dtime, dtime_unit
      print *, "dtime, unit = ", dtime, dtime_unit 
      if (dtime_unit(1:1) == "h") then    ! hours
        stepnr = dtime*3600/dt
      else if (dtime_unit(1:1) == "d") then  ! days
        stepnr = dtime*86400/dt
      else
        write(*,*) "Illegal time unit in particle release file"
        stop
      end if

    else
      write(*,*) "Error in time line in particle release file"
      stop
    end if

  end subroutine get_time

  !
  ! -----------------------------------     
  !     
  subroutine get_pos(pos_line, x, y, z)
    character(len=*) :: pos_line    ! The position line as a string
    real, intent(out) :: x, y, z  ! The position

    if (pos_line(1:1) == 'G') then  ! grid coordinates
      read(pos_line(2:), *) x, y, z
       
    else if (pos_line(1:1) == 'S') then  ! spherical coordinates
      print *, "particles.get_pos: spherical coordinates are not implemented"
      stop

    else
      write(*,*) "Error in position line in particle release file"
      stop
    end if
!FV    print *, "get_pos: x,y,z = ", x, y, z      
  end subroutine get_pos
    


end module particles

