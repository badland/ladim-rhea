module grid

  use setup
!  use ncutil, only : nf90_open, nf90_close, check_err, dimlen, getvar,   &
!                     NF90_NOWRITE, getvartime
  use ncutil

  implicit none

  integer :: imax  ! Number of grid cells in X-direction
  integer :: jmax  ! Number of grid cells in Y-direction
  integer :: kmax  ! Number of vertical levels

  real, dimension(:,:), allocatable   :: H, mask_rho
  real, dimension(:,:), allocatable   :: lon_rho, lat_rho
  real, dimension(:,:), allocatable   :: pm, pn
  real, dimension(:,:,:), allocatable :: depth
  real, dimension(:,:,:), allocatable :: TEMP, SALT
  real, dimension(:), allocatable :: hc  ! depth levels
  real :: distance

  public initgrid

  contains
  !
  ! ****************************************************
  !
  subroutine initgrid(gridfile, datafile1)  

    character(len=*), intent(in) :: gridfile
    character(len=*), intent(in) :: datafile1   ! Some info not in grid file

    integer :: ncid, ncid1     ! NetCDF file identifiers
    integer :: status          ! NetCDF error status

!    real, dimension(:), allocatable :: hc  ! depth levels
    integer :: k,i,j

    print *, "Initgrid: starter"

    ! Open the file
    status = nf90_open(gridfile, NF90_NOWRITE, ncid)
    call check_err(status)

    status = nf90_open(datafile1, NF90_NOWRITE, ncid1)
    call check_err(status)
    
    
    ! Get the problem size
    imax = dimlen(ncid, "xi_rho") - 2
    print *, "imax = ", imax

    jmax = dimlen(ncid, "eta_rho") - 2
    print *, "jmax = ", jmax

    kmax = dimlen(ncid1, "zlevels")
    print *, "kmax = ", kmax

    ! Allocate the arrays
    allocate(H(0:imax+1, 0:jmax+1))
    allocate(hc(1:kmax))
    allocate(lon_rho(0:imax+1, 0:jmax+1), lat_rho(0:imax+1, 0:jmax+1))
    allocate(depth(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(pm(0:imax+1, 0:jmax+1), pn(0:imax+1, 0:jmax+1))
    allocate(TEMP(0:imax+1,0:jmax+1,1:kmax), SALT(0:imax+1,0:jmax+1,1:kmax))
    allocate(mask_rho(0:imax+1,0:jmax+1))

    ! Read the 1D arrays
    call getvar(ncid1, "zlevels", hc)
    print *, 'hc = ', hc

    ! Read the 2D arrays
    call getvar(ncid, "h",        H)
    call getvar(ncid, "mask_rho", mask_rho)
    call getvar(ncid, "lon_rho",  lon_rho)
    call getvar(ncid, "lat_rho",  lat_rho)

    ! Set a negative depth value  on land
     where (mask_rho < 1)
       H = -99
     end where


    ! Read the 3D arrays
!    call getvartime(ncid1, "temp", 1, TEMP)
!    call getvartime(ncid1, "salt", 1, SALT)

! Finner mask_rho fra et vilk�rlig temperatur felt hvor verdier 
! p� land er satt til -32767
!    do i = 0, imax + 1
!      do j = 0, jmax + 1
!        do k = 1, kmax
!          if (TEMP(i,j,k) == -32767) then
!            mask_rho(i,j,k) = 0
!          else
!            mask_rho(i,j,k) = 1
!          endif
!        end do
!      end do
!    end do

! Compute the vertical coordinate structure
    do i = 0, imax + 1
      do j = 0, jmax + 1
       depth(i, j, :)   = -hc(:)
      end do
    end do

! Beregner pm (= 1/dy) og pn (=1/dx) hvor dist er m!
!    do i = 0, imax + 1
!    do j = 0, jmax + 1
    do i = 0, imax
    do j = 0, jmax
     call dist(lon_rho(i,j),lat_rho(i,j),lon_rho(i,j+1),lat_rho(i,j+1),distance)
     pm(i,j+1) = 1.0/(distance*1000.0)
     call dist(lon_rho(i,j),lat_rho(i,j),lon_rho(i+1,j),lat_rho(i+1,j),distance)
     pn(i+1,j) = 1.0/(distance*1000.0)
    end do
    end do    
    pm(:,0) = pm(:,1)
    pm(imax+1,:) = pm(imax,:)
    pn(0,:) = pn(1,:)
    pn(:,jmax+1) = pn(:,jmax)

    !
    ! Clean up
    ! 
    status = nf90_close(ncid)

    print *, "initgrid: ferdig"

  end subroutine initgrid

  !
  ! ****************************************************
  !
  subroutine dist(lon0,lat0,lon1,lat1,distance)  

! Compute distances on a spherical earth with Haversine formula
! tatt fra m_lldist.m i Pawlowicz m_map pakke for Matlab

     real, intent(in)  :: lon0, lat0, lon1, lat1
     real, intent(out) :: distance

    real, parameter :: R_earth = 6371 ! Earth radius [km]
    real :: pi
    real :: rad
    real :: deg

    real :: phi0, phi1, dphi, dlambda, a

    pi = acos(-1.0)   !!  = 3.14159... 
    rad = pi/180.0
    deg = 180.0/pi
    phi0     = lat0*rad
    phi1     = lat1*rad
    dphi     = phi1 - phi0
    dlambda  = (lon1 - lon0) * rad
    a        = sin(0.5*dphi)**2 + cos(phi0)*cos(phi1)*(sin(0.5*dlambda)**2)
    distance = 2 * R_earth * atan2(sqrt(a), sqrt(1-a))
  end subroutine dist

end module grid
