module generate_rand_number

  contains

!     -------------------------------------------------
      subroutine gran(random)
!     -------------------------------------------------
!
!***BEGIN PROLOGUE GRAN
!***DATE WRITTEN    25 April 1993
!***LAST MODIFIED   02 January 1999
!***AUTHOR
!            Bj�rn �dlandsvik
!            Institute of Marine research
!            Postbox 1870 Nordnes
!            N-5024 Bergen, Norway
!            Internet: bjorn@imr.no
!
!***PURPOSE GRAN
!  Compute a normal distributed random number  
!  with zero mean and unit variance.
!
!
!***DESCRIPTION GRAN
!  Uses the Box-Muller transform to compute two gaussian random
!  numbers from uniformly distributed random numbers generated
!  by the internal subroutine RANDOM_NUMBER.
!
!  One of the numbers is returned, while the other is stored
!  for the next call to GRAN.
!  The standard uniform random number generator random_number
!  is
!
!  This routine is coded in standard FORTRAN 77 except for the
!  use of the non-standard internal function RAND.
!
!
!***REFERENCES
!  Press, Flannery, Teukolsky, Vetterling:
!    Numerical Recipes,
!    The Art of Scientific Computing
!      (FORTRAN Version)
!    Cambridge University Press 1989
!
!
! --- Arguments.
!
    implicit none   
    real :: random
    logical, save :: have_value = .FALSE.    ! Flag for existence of already computed value
    real, dimension(2) :: v    ! Uniformly distributed random vector
    real :: r                  ! Square of the length of the vector v
    real :: bmfac              ! Multiplicative factor in Box-Muller transform
    real, save :: stored_value ! Storage for extra random number
!
!***END DECLARATIONS GRAN
!
  if (have_value) then
    have_value = .FALSE.
    random = stored_value
  else
    have_value = .TRUE.
!
!
! Compute a uniformly distributed random vector
! on the punctured unit disc by rejection.
! 
    do
      call random_number(v)  ! Uniform on [0, 1] x [0, 1]
      v = 2.0 * v - 1.0      ! Uniform on [-1,1] x [-1,1]
      r = v(1)**2 + v(2)**2
      if ((r < 1.) .AND. (r > 0.)) exit ! in punctured disc 
    end do                   

!
! --- Box-Muller transform.
!
    bmfac = sqrt(-2.0*log(r)/r)
    random        = v(1)*bmfac
    stored_value  = v(2)*bmfac
!
  end if ! (have_value
!
  end subroutine gran

end module generate_rand_number
