module ncutil

! Utilities for NetCDF handling
!
! This was written to simplify use of the Fortran 77 NF functions
! Changed to fortran 90 interface, June 2014
! Unecessary complicated now as the nf90 interface is more generic



!  private
 
  !include "netcdf.inc"
  use netcdf

  ! Generic interface for reading a variable
  interface getvar
    module procedure getvar0d, getvar1d, getvar2d, getvar3d
  end interface getvar

  ! Generic interface for reading a variable at a specific time step
  interface getvartime
    module procedure getvar0dtime, getdouble0dtime, getvar2dtime, getvar3dtime
  end interface getvartime

!  public check_err, dimlen, getvar, getvartime
!  public nf_open, nf_close
!  public NF_NOWRITE

  contains  

  !
  ! -----------------------------------------------
  !
  subroutine check_err(status)
  ! NetCDF file operation error handling
    integer, intent(in) :: status
    if (status .ne. NF90_NOERR) then
      print *, "***NetCDF error, program terminating"
      print *, "NetCDF error message:"
      print *, "  ", nf90_strerror(status)
      stop
    endif
  end subroutine check_err
  !
  ! ------------------------------------
  !
  function dimlen(ncid, dimname)
    ! Returns the length of a NetCDF dimension
    integer :: dimlen
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: dimname
    integer :: dimid
    integer :: status

    status = nf90_inq_dimid(ncid, dimname, dimid)
    if (status == NF90_EBADDIM) then
      write(*,*) "***Wrong dimension name: ", dimname
    end if
    call check_err(status)
    status = nf90_inquire_dimension(ncid, dimid, len=dimlen)
    call check_err(status)
  end function dimlen
  ! 
  ! --------------------------------------------
  ! 
  subroutine getvar0d(ncid, vname, value)
  ! Read a scalar value from the NetCDF file
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    real, intent(out) :: value
    integer :: varid
    integer :: status

    status = nf90_inq_varid(ncid, vname, varid)
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    status = nf90_get_var(ncid, varid, value)
    call check_err(status)
  end subroutine getvar0d

  subroutine getvar1d(ncid, vname, field)
  ! Read a 1D field from the NetCDF file
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    real, dimension(:), intent(out) :: field
    integer :: varid
    integer :: status

    status = nf90_inq_varid(ncid, vname, varid)
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    status = nf90_get_var(ncid, varid, field)
    call check_err(status)
  end subroutine getvar1d
  !
  ! -------------------------------------------------------
  !
  subroutine getvar2d(ncid, vname, field)
    ! Read a 2D field from the NetCDF file
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    real, dimension(:,:), intent(out) :: field
    integer :: varid
    integer :: status

    status = nf90_inq_varid(ncid, vname, varid)
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    status = nf90_get_var(ncid, varid, field)
    call check_err(status)
  end subroutine getvar2d
  !
  ! -------------------------------------------------------
  !
  subroutine getvar3d(ncid, vname, field)
    ! Read a 3D field from the NetCDF file
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    real, dimension(:,:,:), intent(out) :: field
    integer :: varid
    integer :: status

    status = nf90_inq_varid(ncid, vname, varid)
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    status = nf90_get_var(ncid, varid, field)
    call check_err(status)
  end subroutine getvar3d
  
!
! --------------------------------------------
!
  subroutine getvar0dtime(ncid, vname, timestep, field)
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    integer, intent(in) :: timestep
    real, intent(out) :: field
    integer :: varid
    integer, dimension(1) :: start, count
    integer :: status

    start = (/ timestep /)
    count = (/ 1 /)
    status = nf90_inq_varid(ncid, vname, varid)
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    !status = nf90_get_var(ncid, varid, field, start, count)
    status = nf90_get_var(ncid, varid, field, start)
    call check_err(status)
  end subroutine getvar0dtime

  subroutine getdouble0dtime(ncid, vname, timestep, field)
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    integer, intent(in) :: timestep
    real(kind=8), intent(out) :: field
    integer :: varid
    integer, dimension(1) :: start, count
    integer :: status

    start = (/ timestep /)
    count = (/ 1 /)

    status = nf90_inq_varid(ncid, vname, varid)
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    !status = nf90_get_var(ncid, varid, field, start, count)
    status = nf90_get_var(ncid, varid, field, start)
    call check_err(status)
  end subroutine getdouble0dtime

  subroutine getvar2dtime(ncid, vname, timestep, field)
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    integer, intent(in) :: timestep
    real, dimension(:,:), intent(out) :: field
    integer :: varid
    integer, dimension(3) :: start, count
    integer :: status
    start = (/ 1, 1, timestep /)
!    print *, 'shape = ', shape(field)
!    print *, 'start = ', start
    count(1:2) = shape(field) ! a rank-one array where each element is the extent of the corresponding dim.
    count(3) = 1

    status = nf90_inq_varid(ncid, vname, varid)
!    print *, 'ncid, vname, varid = ', ncid, vname, varid
!    print *, 'NF90_ENOTVAR, status = ',NF90_ENOTVAR, status
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
!    status = nf90_get_var_real(ncid, varid, field)
    status = nf90_get_var(ncid, varid, field, start, count)
    call check_err(status)
!    print *, 'end getvar2dtime'
  end subroutine getvar2dtime

  subroutine getvar3dtime(ncid, vname, timestep, field)
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: vname
    integer, intent(in) :: timestep
    real, dimension(:,:,:), intent(out) :: field
    integer :: varid
    integer, dimension(4) :: start, count
    integer :: status
    start = (/ 1, 1, 1, timestep /)
!    print *, 'start = ', start
    count(1:3) = shape(field)
!    print *, 'shape = ', shape(field)
    count(4) = 1

    status = nf90_inq_varid(ncid, vname, varid)
!    print *, 'ncid, vname, varid = ', ncid, vname, varid
!    print *, 'NF90_ENOTVAR, status = ',NF90_ENOTVAR, status
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    status = nf90_get_var(ncid, varid, field, start, count)
    call check_err(status)
  end subroutine getvar3dtime
  
  !
  ! --------------------------------------------------------
  !  
  subroutine getatt(ncid, varname, attname, attstr)
  ! Get a string attribute
    integer, intent(in) :: ncid
    character(len=*), intent(in) :: varname
    character(len=*), intent(in) :: attname
    character(len=*), intent(out) :: attstr

    integer :: varid, attid, xtype
    integer :: status

    status = nf90_inq_varid(ncid, varname, varid)
    if (status == NF90_ENOTVAR) then
      write(*,*) "***Wrong variable name: ", vname
    end if
    call check_err(status)
    
!    status = nf90_inq_attid(ncid, varid, attname, attid)
!    if (status == NF90_ENOTATT) then
!      write(*,*) "***Variable: ", varname, " has no attribute ", attname
!    end if
!    call check_err(status)

    status = nf90_get_att(ncid, varid, attname, attstr)
    if (status == NF90_ENOTATT) then
      write(*,*) "***Variable: ", varname, " has no attribute ", attname
    end if
    if (status == NF90_EBADTYPE) then
      write(*,*) "***Attribute", attname, " to variable ", varname,  &
                     "is not text"
    end if
    call check_err(status)

  end subroutine getatt
    


end module ncutil



