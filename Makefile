PROG   = ladim

#HEAD = cppdefs.h

SRC = ladim.f90 ncutil.f90 grid.f90 input.f90 sample.f90  \
       outnc.f90 setup.f90 particles.f90 time_module.f90   \
       ibm.f90 trackpart.f90 surface_light.f90 generate_rand_number.f90
MISC = part.ini ladim.sup

# Intel compiler on rhea
FC     = /opt/intel/bin/ifort
#FFLAGS = -O2 
FFLAGS = -O2 -fp-stack-check -fpe0 -C

# Intel for error check
# FFLAGS = -check all -warn all,nodec,interfaces -gen_interfaces -traceback -fpe0 -fp-stack-check


OBJ = $(SRC:.f90=.o) 

# NetCDF
LIB_NC = -L/opt/netcdf/netcdf-4.3.3.1/lib/ -L/opt/netcdf/netcdf-fortran-4.4.2/lib/ -L/opt/hdf5/hdf5-1.8.14/lib/ -lnetcdf -lnetcdff -lhdf5
INC_NC = -I/opt/netcdf/netcdf-4.3.3.1/include/ -I/opt/netcdf/netcdf-fortran-4.4.2/include/ -I/opt/hdf5/hdf5-1.8.14/include/

INTEL_LIBS = -limf -lsvml -lirng -lintlc -lifport -lifcore 

####


all: $(PROG)

.SUFFIXES:
.SUFFIXES: .f90 .o

.f90.o: 
	$(FC) -c $(FFLAGS) $(INC_NC) $<
.F90.o: 
	$(FC) -c $(FFLAGS) -fpp $(INC_NC) $<

$(PROG): $(OBJ)
	 $(FC) $(FFLAGS) -o $(PROG) $(OBJ) $(LIB_NC) $(INTEL_LIBS)

#### YMSE ########

# Opprydding
clean:
	rm -f $(OBJ) $(PROG) *.mod
love:
	@echo "not war"

# Dependencies
# time_module.o : 
setup.o       : time_module.o
grid.o        : setup.o ncutil.o
sample.o      : grid.o
input.o       : grid.o ncutil.o time_module.o setup.o
particles.o   : setup.o input.o sample.o time_module.o generate_rand_number.o
outnc.o       : setup.o grid.o particles.o ncutil.o sample.o input.o ibm.o
trackpart.o   : setup.o grid.o particles.o input.o sample.o generate_rand_number.o
ibm.o         : particles.o grid.o sample.o input.o setup.o time_module.o surface_light.o generate_rand_number.o
ladim.o       : setup.o grid.o particles.o input.o outnc.o trackpart.o ibm.o time_module.o
# generate_rand_number.o :
# surface_light.o :
