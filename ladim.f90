program ladim
!
!******************************************************
!  LADIM --- Lagrangian Advection and DIfusion Model  *
! --------------------------------------------------- *
!                                                     *
!  Author: Bj�rn �dlandsvik                           *
!          Institute of Marine Research               *
!          P.O.Box 1870, Nordnes                      *
!          N-5024 Bergen, Norway                      *
!          E-mail: bjorn@imr.no                       *
!                                                     *
!******************************************************
!


  use setup,            only : readsup,                                   &
                               bfnavn, fname, partfilename, utfnavn,      &
                               nstep, dt, outper, start_time
  use grid,             only : initgrid, imax, jmax!, cop_dens
  use particles,        only : init_particles, release_particles,        & 
                               read_next_particles,ant_anlegg,           &
                               Xpos, Ypos, Zpos, mult_ant, dt_dager!, release_step
  use input,            only : init_input, get_input, update_input,        &
                               input_step
  use trackpart,        only : movepart2
  use outnc,            only : init_output, write_output, close_output
  use ibm,              only : louse_behaviour
  use time_module

  implicit none

  real, parameter :: sec2day = 1.0/86400.0

  integer :: step          ! step counting index variable
  integer :: outcount      ! number of times output is written
  real :: latime           ! Time in decimal days since initialization
  type(time_type) :: ladim_time  ! Real date and clock
  real :: input_time       ! Time since input updatet IAJ 1. june 12

integer :: ii, jj
!integer,  dimension(imax,jmax) :: COP_DENS
integer :: file_id = 987 ! for partikkel-input-fila
   
  ! BAA: 2013-09-18
  integer :: iarg        ! Number of command line arguments
  character(len=32) :: supfilename  ! Name of setup-file

!IAJ: Rydda lesing av partikkelfil fra ladim.sup og div 10. April

!
!***FIRST EXECUTABLE STATEMENT LADIM
!
!

  write(*,*) ' '
  write(*,*) '****************************************************'
  write(*,*) ' LADIM --- Lagrangian Advection and DIfusion Model'
  write(*,*) ' Version 2.0'
  write(*,*) '****************************************************'
  write(*,*) ' '

!
! --- Initiate random number generator
! Trengs bare hvis RANDOM_WALK
  call random_seed()
  
!
! --- Read Setup-file.
!
!  call readsup()

  iarg = command_argument_count()
  if (iarg == 0) then
    supfilename = "ladim.sup"
  else
    call get_command_argument(1, supfilename)
  endif
  call readsup(supfilename)

!
! --- Initialize the grid structure
!
  call initgrid(bfnavn, fname)

!
! --- Open input file
  call init_input()

!
! --- Open particle init file
  call init_particles(partfilename, file_id)

!
! --- Initialise timers
!
  outcount   = 0
  latime     = 0.0
  step = 0
  ladim_time = start_time
! IAJ teller tid siden oppdatet input
  input_time = 0.0
  ! - Write time info 
  write(*,'(I6,X,F10.4,3X,A)')  step, latime, timestr(ladim_time)

! 
! --- Initial particle release
!LarsFEB12
!Lars  if (release_step == 0) then
    call release_particles(ant_anlegg,Xpos,Ypos,Zpos,mult_ant)
!  end if
!LarsFEB12

!
! --- Initialize NetCDF output file
!
  call init_output(utfnavn)

!
! --- Write initial particle distribution
!
  call write_output(latime)
    print *,  'LADIM: step, time = ', step, ladim_time

! ----------------------------------------
! --- Main time loop 
! ----------------------------------------

  do step = 1, nstep

    ! - Update timers
    latime = step * dt * sec2day
    ladim_time = ladim_time + dt * sec2day
    input_time = input_time + dt * sec2day

!
!Lars    if (step == release_step) then
! Slipper ut hver time
!
    if (mod(step, int(3600/dt)) == 0) then
      write (*, *) "  Particle release"
!LarsFEB12
      call release_particles(ant_anlegg,Xpos,Ypos,Zpos,mult_ant)
!LarsFEB12
    end if 

!LarsFEB12
    !IAJ APR 12
!IAJ juni 12    if (latime >= 1 .and. mod(int(latime), dt_dager) == 0) then
    if (input_time>=dt_dager) then
       call read_next_particles(file_id)
       input_time = 0.0
    end if
!LarsFEB12

!
!   --- Time to read in a new field ?
!
    if (step > input_step) then
      print *, "  Input time"
      call get_input()
    end if

!
!   --- Update velocity.
!  (skal dette gj�res n�r lest str�m ?? Ja
    call update_input()

!
!   --- Move the particles.
!
    call movepart2()

! 
!   ---- Vertical behaviour
!

    call louse_behaviour(latime,ladim_time)

!
!  --- Output ?
!
    if (mod(step, outper) == 0) then  ! Write out
      outcount = outcount + 1
      write(*,*) '  Output'
      call write_output(latime)
    end if

  end do     ! End TIME LOOP

!
! --- Close NetCDF file
!
  call close_output()

! Skriver ut til fortran.889

!do jj = 1,jmax !JM
!do ii = 1,imax !IM
!write(889,*) cop_dens(ii,jj)
!enddo
!enddo


  write(*,*) ' '
  write(*,*) ' LADIM finished normally '
  write(*,*) ' '
  write(*,*) ' Written ', outcount, ' particle distributions'

end program ladim
