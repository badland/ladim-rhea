module sample

  use grid, only : imax, jmax, kmax, depth

  contains

  !
  ! -----------------------------------------------
  !
  subroutine sample_2d_rho(A, x, y, val)
    ! Bilinear sampling
 
    ! Must have 0 <= x < imax+1, 
    !           0 <= y < jmax+1

    real, dimension(0:imax+1, 0:jmax+1), intent(in) :: A
    real, intent(in) :: x, y
    real, intent(out) :: val

    integer :: i, j, k
    real :: p, q, r
    real :: a00, a01, a10, a11

    integer :: k0   


    ! Fint rho-point south-west of (x,y)
    i = int(x)
    j = int(y)
    ! Compute local coordinates
    p = x - i
    q = y - j

    a00 = A(i, j)
    a01 = A(i, j+1)
    a10 = A(i+1, j)
    a11 = A(i+1, j+1)
    ! Bilinear
    val = a00 + p * (a10-a00) + q * (a01-a00) + p*q*(a11-a01-a10+a00)

  end subroutine sample_2d_rho
  !
  ! ------------------------------------------------
  !

  !BAA-2015-04-14, nearest neighbour horizontally
  !                nearest value above, vertically
  subroutine sample_3d_rho(A, x, y, z, val)
    !
    ! Samples a 3D field given in rho-points
    !

    real, dimension(0:imax+1, 0:jmax+1, kmax), intent(in) :: A
      ! 3D field to be sampled
    real, intent(in) :: x, y, z      ! Sampling location
    real, intent(out) :: val         ! Sampled value

    real, dimension(size(A,3)) :: D  ! Local depth structure
    integer :: i, j, k               ! Grid cell counters
    real :: p, q, r                  ! Local coordinates
    real :: w00,  w01,  w10,  w11    ! 2D interpolation weights
    real :: w000, w001, w010, w011   ! 3D interpolation weights
    real :: w100, w101, w110, w111   !       ---- " ----

    ! Local horizontal coordinates
    i = nint(x)
    j = nint(y)

    D = Depth(i, j, :)

    ! Find the local depth level,
    if (z>0 .or. z<=D(kmax)) then
    print *, "illegal z value = ", z
      stop
    end if
    ! Find largest k s.t. D(k) >= z  
    !      
    do k = 1, kmax-1
      if (D(k+1) < z) exit
    end do

    val = A(i, j, k)

    
  end subroutine sample_3d_rho

  ! Tri-linear interpolation
  subroutine sample_3d_rho1(A, x, y, z, val)
    !
    ! Samples a 3D field given in rho-points
    !
    ! trilinear interpolation, horizontally along s-coordinate
    ! Must have 0 <= x < imax+1
    !           0 <= y < jmax+1
    ! Any z-value is legal, but extends constantly below lowest
    ! and above highest s-level
    !

    real, dimension(0:imax+1, 0:jmax+1, kmax), intent(in) :: A
      ! 3D field to be sampled
    real, intent(in) :: x, y, z      ! Sampling location
    real, intent(out) :: val         ! Sampled value


    real, dimension(size(A,3)) :: D  ! Local depth structure
    real, dimension(size(A,3)) :: ONES ! brukes i s�ekrutine
    integer :: i, j, k               ! Grid cell counters
    integer, dimension(1) :: Iover   ! Finner k-indeks over og under z-niv�
    real :: p, q, r                  ! Local coordinates
    real :: w00,  w01,  w10,  w11    ! 2D interpolation weights
    real :: w000, w001, w010, w011   ! 3D interpolation weights
    real :: w100, w101, w110, w111   !       ---- " ----

    ONES =1 

    ! Local horizontal coordinates
    i = int(x)
    j = int(y)

    !!! B�r sjekke at innenfor omr�det
    p = x - i
    q = y - j
    ! Horizontal intepolation weights
    w00 = (1-p)*(1-q)
    w01 = (1-p)*q
    w10 = p*(1-q)
    w11 = p*q

    ! interpolate local depth structure
    do k = 1, kmax
       D(k) = w00 * Depth(i,   j, k)  +  w01 * Depth(i,   j+1, k) +    &
              w10 * Depth(i+1, j, k)  +  w11 * Depth(i+1, j+1, k)
    end do

    ! Find the local s-coordinate,
    if (z>0 .or. z<=-1000) then
    print *, "z must be in the interval [0,1000> "
      stop
    end if
    Iover = minloc(sign(ONES,D-z)) - 1 
    k = Iover(1)
    r = (z-D(k))/(D(k+1)-D(k))

    ! trilinear interpolation
    w000 = w00*(1-r)
    w001 = w00*r
    w010 = w01*(1-r)
    w011 = w01*r
    w100 = w10*(1-r)
    w101 = w10*r
    w110 = w11*(1-r)
    w111 = w11*r

    val = w000 * A(i,   j,   k) + w001 * A(i,   j,   k+1) +       &
          w010 * A(i,   j+1, k) + w011 * A(i,   j+1, k+1) +       &
          w100 * A(i+1, j,   k) + w101 * A(i+1, j,   k+1) +       &
          w110 * A(i+1, j+1, k) + w111 * A(i+1, j+1, k+1) 

  end subroutine sample_3d_rho1

  !
  ! ------------------------------------------------------
  !
  subroutine sample_UV(U, V, x, y, z, u0, v0)
    !
    ! Samples a 3D staggered velocity field 
    !
    ! trilinear interpolation, horizontally along s-coordinate
    ! Must have 0.5 <= x < imax+0.5
    !           0.5 <= y < jmax+0.5
    !          (men dette sjekkes ikke)
    ! Any z-value is legal, but extends constantly below lowest
    ! and above highest s-level
    !

    real, dimension(1:imax+1, 0:jmax+1, kmax), intent(in) :: U
    real, dimension(0:imax+1, 1:jmax+1, kmax), intent(in) :: V
      ! 3D field to be sampled
    real, intent(in) :: x, y, z      ! Sampling location
    real, intent(out) :: u0, v0      ! Sampled values


    real, dimension(size(U,3)) :: D  ! Local depth structure
    real, dimension(size(U,3)) :: ONES ! brukes i s�ekrutine
    integer :: i, j                  ! rho grid 
    integer :: iu                    ! u grid
    integer :: jv                    ! v grid
    integer :: k                     ! s-level index
    integer, dimension(1) :: Iover   ! Finner k-indeks over og under z-niv�
    real :: p, q, r                  ! Local coordinates
    real :: pu, qv                   !    ---- " ----  in u and v system
    real :: w00,  w01,  w10,  w11    ! 2D interpolation weights
    real :: w000, w001, w010, w011   ! 3D interpolation weights
    real :: w100, w101, w110, w111   !       ---- " ----

    ONES=1

    ! Local horizontal coordinates
    i  = int(x)
    j  = int(y)
    iu = int(x+0.5)
    jv = int(y+0.5)
    
    !!! B�r sjekke at innenfor omr�det
    p  = x - i
    q  = y - j
    pu = x - iu + 0.5
    qv = y - jv + 0.5 

    ! Horizontal intepolation weights
    w00 = (1-p)*(1-q)
    w01 = (1-p)*q
    w10 = p*(1-q)
    w11 = p*q

    ! interpolate local depth structure
    do k = 1, kmax
       D(k) = w00 * Depth(i,   j, k)  +  w01 * Depth(i,   j+1, k) +    &
              w10 * Depth(i+1, j, k)  +  w11 * Depth(i+1, j+1, k)
    end do

    ! Find the local s-coordinate,
    if (z>0 .or. z<=-1000) then
    print *, "z must be in the interval [0,1000> "
      stop
    end if
    Iover = minloc(sign(ONES,D-z)) - 1 
    k = Iover(1)
    r = (z-D(k))/(D(k+1)-D(k))

    ! trilinear interpolation for U
    w00  = (1-pu)*(1-q)
    w01  = (1-pu)*q
    w10  = pu*(1-q)
    w11  = pu*q
    w000 = w00*(1-r)
    w001 = w00*r
    w010 = w01*(1-r)
    w011 = w01*r
    w100 = w10*(1-r)
    w101 = w10*r
    w110 = w11*(1-r)
    w111 = w11*r

    u0 = w000 * U(iu,   j,   k) + w001 * U(iu,   j,   k+1) +       &
         w010 * U(iu,   j+1, k) + w011 * U(iu,   j+1, k+1) +       &
         w100 * U(iu+1, j,   k) + w101 * U(iu+1, j,   k+1) +       &
         w110 * U(iu+1, j+1, k) + w111 * U(iu+1, j+1, k+1) 

    ! trilinear interpolation for V
    w00  = (1-p)*(1-qv)
    w01  = (1-p)*qv
    w10  = p*(1-qv)
    w11  = p*qv
    w000 = w00*(1-r)
    w001 = w00*r
    w010 = w01*(1-r)
    w011 = w01*r
    w100 = w10*(1-r)
    w101 = w10*r
    w110 = w11*(1-r)
    w111 = w11*r

    v0 = w000 * V(i,   jv,   k) + w001 * V(i,   jv,   k+1) +       &
         w010 * V(i,   jv+1, k) + w011 * V(i,   jv+1, k+1) +       &
         w100 * V(i+1, jv,   k) + w101 * V(i+1, jv,   k+1) +       &
         w110 * V(i+1, jv+1, k) + w111 * V(i+1, jv+1, k+1) 


  end subroutine sample_UV

  subroutine sample_UV2(U, V, DU, DV, x, y, z, u0, v0, tfrac)
    !
    ! Samples a 3D staggered velocity field 
    !
    ! trilinear interpolation, horizontally along s-coordinate
    ! Must have 0.5 <= x < imax+0.5
    !           0.5 <= y < jmax+0.5
    !          (men dette sjekkes ikke)
    ! Any z-value is legal, but extends constantly below lowest
    ! and above highest s-level
    !

    real, dimension(1:imax+1, 0:jmax+1, kmax), intent(in) :: U
    real, dimension(0:imax+1, 1:jmax+1, kmax), intent(in) :: V
    real, dimension(1:imax+1, 0:jmax+1, kmax), intent(in) :: DU
    real, dimension(0:imax+1, 1:jmax+1, kmax), intent(in) :: DV
      ! 3D field to be sampled
    real, intent(in) :: x, y, z      ! Sampling location
    real, intent(out) :: u0, v0      ! Sampled values
    real, intent(in) :: tfrac ! fraction of time step
      ! tfrac = 0.0 for current time step
      ! tfrac = 1.0 for next time step

    real, dimension(size(U,3)) :: D  ! Local depth structure
    real, dimension(size(U,3)) :: ONES ! brukes i s�ekrutine
    integer :: i, j                  ! rho grid 
    integer :: iu                    ! u grid
    integer :: jv                    ! v grid
    integer :: k                     ! s-level index
    integer, dimension(1) :: Iover   ! Finner k-indeks over og under z-niv�
    real :: p, q, r                  ! Local coordinates
    real :: pu, qv                   !    ---- " ----  in u and v system
    real :: w00,  w01,  w10,  w11    ! 2D interpolation weights
    real :: w000, w001, w010, w011   ! 3D interpolation weights
    real :: w100, w101, w110, w111   !       ---- " ----

    ONES=1

    ! Local horizontal coordinates
    i  = int(x)
    j  = int(y)
    iu = int(x+0.5)
    jv = int(y+0.5)
   
    !!! B�r sjekke at innenfor omr�det
    p  = x - i
    q  = y - j
    pu = x - iu + 0.5
    qv = y - jv + 0.5 

    ! Horizontal intepolation weights
    w00 = (1-p)*(1-q)
    w01 = (1-p)*q
    w10 = p*(1-q)
    w11 = p*q

    ! interpolate local depth structure
    do k = 1, kmax
       D(k) = w00 * Depth(i,   j, k)  +  w01 * Depth(i,   j+1, k) +    &
              w10 * Depth(i+1, j, k)  +  w11 * Depth(i+1, j+1, k)
    end do

    !write (*,*) 'z = ', z
    if (z>0 .or. z<=-1000) then
    print *, "z must be in the interval [0,1000> "
      stop
    end if
    Iover = minloc(sign(ONES,D-z)) - 1 
    k = Iover(1)
    r = (z-D(k))/(D(k+1)-D(k))

    ! trilinear interpolation for U
    w00  = (1-pu)*(1-q)
    w01  = (1-pu)*q
    w10  = pu*(1-q)
    w11  = pu*q
    w000 = w00*(1-r)
    w001 = w00*r
    w010 = w01*(1-r)
    w011 = w01*r
    w100 = w10*(1-r)
    w101 = w10*r
    w110 = w11*(1-r)
    w111 = w11*r

    u0 = w000 * U(iu,   j,   k) + w001 * U(iu,   j,   k+1) +       &
         w010 * U(iu,   j+1, k) + w011 * U(iu,   j+1, k+1) +       &
         w100 * U(iu+1, j,   k) + w101 * U(iu+1, j,   k+1) +       &
         w110 * U(iu+1, j+1, k) + w111 * U(iu+1, j+1, k+1) +       &
  tfrac*(w000 * DU(iu,   j,   k) + w001 * DU(iu,   j,   k+1) +       &
         w010 * DU(iu,   j+1, k) + w011 * DU(iu,   j+1, k+1) +       &
         w100 * DU(iu+1, j,   k) + w101 * DU(iu+1, j,   k+1) +       &
         w110 * DU(iu+1, j+1, k) + w111 * DU(iu+1, j+1, k+1)    )

    ! trilinear interpolation for V
    w00  = (1-p)*(1-qv)
    w01  = (1-p)*qv
    w10  = p*(1-qv)
    w11  = p*qv
    w000 = w00*(1-r)
    w001 = w00*r
    w010 = w01*(1-r)
    w011 = w01*r
    w100 = w10*(1-r)
    w101 = w10*r
    w110 = w11*(1-r)
    w111 = w11*r

    v0 = w000 * V(i,   jv,   k) + w001 * V(i,   jv,   k+1) +       &
         w010 * V(i,   jv+1, k) + w011 * V(i,   jv+1, k+1) +       &
         w100 * V(i+1, jv,   k) + w101 * V(i+1, jv,   k+1) +       &
         w110 * V(i+1, jv+1, k) + w111 * V(i+1, jv+1, k+1) +       &
  tfrac*(w000 * DV(i,   jv,   k) + w001 * DV(i,   jv,   k+1) +       &
         w010 * DV(i,   jv+1, k) + w011 * DV(i,   jv+1, k+1) +       &
         w100 * DV(i+1, jv,   k) + w101 * DV(i+1, jv,   k+1) +       &
         w110 * DV(i+1, jv+1, k) + w111 * DV(i+1, jv+1, k+1)   )


  end subroutine sample_UV2




  !
  ! ------------------------------------------
  ! 
  function hsearch(X, n, x0, k0) result(k)
  !
  ! Given an sorted array X with n elements,
  ! find k such that
  !    X(k) <= x0 < X(k+1), k = 1, ..., n-1
  ! if x0 < X(1)  return k = 0
  ! if x0 >= X(n) return k = n
  !
  ! k0 is an initial guess for k
  ! 
  ! Modified from the hunt search routine in 
  ! Numerical Recipes (Press et al.)
  !
 
    integer :: k
    integer, intent(in) :: n
    real, dimension(n), intent(in) :: X
    real, intent(in) :: x0
    integer, intent(in) :: k0
  
    integer :: k1, k2, inc

    if ((k0 <= 0) .or. (k0 > n)) then
      ! k0 is useless
      k = bsearch(0, n+1)
      return
    end if

    if (x0 >= X(k0)) then  ! Hunt upwards     
      inc = 1
      k1 = k0
      do 
        k2 = k0 + inc
        if (k2 > n) then         ! Offrange
          k = bsearch(k1, n+1)
          return
        end if
        if (x0 < X(k2)) then   ! X(k1) <= x0 < X(k2)
          k = bsearch(k1, k2)
          return
        end if
        inc = inc + 1
      end do  

    else ! x0 < X(k0), hunt downwwards
      inc = 1
      k2  = k0
      do 
        k1  = k0 - inc  
        if (k1 < 1) then        ! Offrange
          k = bsearch(0, k2)
          return
        end if
        if (x0 >= X(k1)) then  ! X(k1) <= x0 < X(k2)
          k = bsearch(k1, k2)
          return
        end if
        inc = inc + 1
      end do
    end if
       
    contains

    function bsearch(k1, k2) result(k)
    ! bisection search
      integer :: k
      integer, intent(in) :: k1, k2
      integer :: m1, m2, m

      m1 = k1
      m2 = k2
      do while ((m2 - m1) > 1)
        m = (m1 + m2) / 2
        if (X(m) <= x0) then
          m1 = m
        else
          m2 = m
        end if
      end do
      k = m1
      return
      
    end function bsearch

  end function hsearch
  
end module sample
