module input

  use ncutil, only : nf_open, nf_close, NF_NOWRITE, check_err,    &
                     getatt, getvar, getvartime, dimlen
  use time_module
  use grid, only : imax, jmax, kmax,                              &
                   mask_rho

  use setup 


  implicit none

  integer, dimension(:), allocatable :: ocean_step
  integer :: ocean_l

  real, dimension(:,:,:), allocatable :: U
  real, dimension(:,:,:), allocatable :: V
  real, dimension(:,:,:), allocatable :: TEMP

  real, dimension(:,:,:), allocatable :: DU
  real, dimension(:,:,:), allocatable :: DV
  real, dimension(:,:,:), allocatable :: DTEMP

  real, dimension(:,:,:), allocatable, private :: Unew
  real, dimension(:,:,:), allocatable, private :: Vnew
  real, dimension(:,:,:), allocatable, private :: TEMPnew

  integer :: input_step

  character(len=80), private :: froot  ! File name root
  integer, private  :: filenum    ! Input file number
  integer, private  :: filepos    ! Time step in input file
  integer, private  :: ntimes     ! Number of time steps in input file
  integer, private  :: ncid       ! NetCDF file identifier
  character(len=10), private :: time_units
  type(time_type), private   :: time_origin
  type(time_type), private   :: next_time
  integer, private :: input_step0

  contains

  !
  ! *****************************************************
  ! 
  subroutine init_input

  ! -----
  ! Initializing routine for the input from the ocean model
  !
  ! H�ndterer n� bare serie-filer
  !  lage til en svitsj (f.eks. i setup-rutinen)
  !  slik at kan og kj�re enkelt-filer

    real(kind=8) :: dtime
    type(time_type) :: start_ocean
    type(time_type) :: ocean_time
 !   integer :: i
    integer :: fnumpos   ! position of file number in file name
    character(len=80) :: filename
 !   integer :: step0
    logical :: found_file
    integer :: nfile
    integer :: l   ! time index
    integer :: oldfilepos
    integer :: oldfilenum


    print *, "init_input: startet"

    ! Allocate arrays
    allocate(U(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(V(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(TEMP(0:imax+1, 0:jmax+1, 1:kmax))

    allocate(DU(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(DV(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(DTEMP(0:imax+1, 0:jmax+1, 1:kmax))

    allocate(Unew(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(Vnew(0:imax+1, 0:jmax+1, 1:kmax))
    allocate(TEMPnew(0:imax+1, 0:jmax+1, 1:kmax))


    ! TEST: SERIE FILER ???
    fnumpos = index(fname ,'_0000.nc')
    if ( fnumpos> 0) then
      filenum = start_fnum
      froot = fname(1:fnumpos)  ! file-navn med xxxx_
    else
      filenum = -1 
      filename = fname
    end if
    found_file = .false.
    nfile = filenum


    ! --- LOOP OVER INPUT FILES ---
!FV found_file er false inntil filen er aapnet. .not. found_file er 
!FV derfor lik true
    do while (.not. found_file)
      ! Open the file
      write(filename, '(A,I4.4,A3)') trim(froot), nfile, '.nc'
      call open_input(filename)
      ! --- loop over time steps within the file
      do l = 1, ntimes
        call get_time(l, ocean_time)        
        if (ocean_time > start_time) then 
          if (l > 1) then
            oldfilepos = l - 1
            oldfilenum = nfile
          else
            ! Bactrack a file
!            oldfilepos = -1  ! last position BUG
            oldfilepos = ntimes  ! last position
            oldfilenum = nfile - 1
          end if
          found_file = .true.
          next_time = ocean_time
          filepos = l
          filenum = nfile
          exit
        else
       oldfilepos = ntimes
        end if
      end do ! l
print *, '3'

      call close_input()
      nfile = nfile+1
    end do ! while

    ! Read last field <= start_time
    
    write(filename, '(A,I4.4,A3)') trim(froot), oldfilenum, '.nc'
    call open_input(filename)
    call get_time(oldfilepos, ocean_time)

    ! Beregn tidsteg:
    ! 0 = start, 
    input_step0 = nint( (ocean_time-start_time) * 86400.0 / dt)
    input_step = nint( (next_time-start_time) * 86400.0 / dt)

    print *, "siste tidspunkt <= start_time"
    print *, "input_step0 = ", input_step0
    print *, "next_time   = ", input_step

    ! Read the current at last time <= start_time        
    call read_current(oldfilepos, Unew, Vnew)
    call read_temp(oldfilepos, TEMPnew)

    ! Call first ordinary read
    call get_input()
    
    ! Interpolate to start_time 
    ! Remember that input_step0 <= 0
    U = U - input_step0*DU
    V = V - input_step0*DV
    TEMP = TEMP - input_step0*DTEMP


    print *, "init_input: Finished"


  end subroutine init_input

  !
  ! -------------------------------------------
  !
  subroutine get_input()


!    integer, intent(out) :: itime

    integer :: k
    character(len=100) :: filename

    ! Update the velocity fields
    U = Unew
    V = Vnew
!    W = Wnew
    TEMP = TEMPnew

    print *, "get_input: filepos = ", filepos

    ! Read next current field
    call read_current(filepos, Unew, Vnew)
    call read_temp(filepos, TEMPnew)

    
    ! Compute increments
!     Dette m� fikses
    DU = (Unew - U) / (input_step - input_step0)
    DV = (Vnew - V) / (input_step - input_step0)
!    DW = (Wnew - W) / 
    DTEMP = (TEMPnew - TEMP) / (input_step - input_step0)

    ! Finn neste tidspunkt: next_time
    filepos = filepos + 1
    if (filepos > ntimes) then
      ! Continue with next input file
      call close_input()
      filenum = filenum + 1
      filepos = 1
      write(filename, '(A,I4.4,A3)') trim(froot), filenum, '.nc'
!FV      write(filename, '(A,I3.3,A3)') trim(froot), filenum, '.nc'
      call open_input(filename)
    end if
    call get_time(filepos, next_time)
    input_step0 = input_step
    input_step = nint( (next_time-start_time) * 86400.0 / dt)

  end subroutine get_input

  !
  !
  ! --------------------------------------------
  !
  subroutine open_input(fname)

    character(len=*), intent(in) :: fname

    character(len=80) :: tstring
    integer :: status

    ! Open the NetCDF file
    status = nf_open(fname, NF_NOWRITE, ncid)

    ! Error handling
    if (status /= 0) then
      write(*,*)  " "
      write(*,*) "open_input: can not open NetCDF file: ", fname
      call check_err(status)
    end if

    write(*,*) "opened file: ", fname

    ! Get ntimes, time_units, and time_origin
    ntimes = dimlen(ncid, 'time')
    call getatt(ncid, "time", "units", tstring)
    if (tstring(1:1) == "d") then
      time_units = "days"
      tstring = tstring(12:)
    else if (tstring(1:1) == "h") then
      time_units = "hours"
      tstring = tstring(13:)
    else if (tstring(1:1) == "s") then
      time_units = "seconds"
      tstring = tstring(15:)
    else
      print *, "***Illegal units attribute: ", tstring
      stop
    end if
    time_origin = tstring
  
  end subroutine open_input





  !
  ! -----------------------------------
  !
  subroutine update_input()

    u = u + du
    v = v + dv
!    w = w + dw
    TEMP = TEMP + dTEMP

  end subroutine update_input

  !
  !
  !
  subroutine read_current(l,U,V)
!  subroutine read_current(l,U,V,W)

    integer, intent(in) :: l
    real, dimension(:,:,:), intent(out) :: U
    real, dimension(:,:,:), intent(out) :: V
!    call getvartime(ncid, "x_velocity", l, U)
!    call getvartime(ncid, "y_velocity", l, V)
    call getvartime(ncid, "u", l, U)
    call getvartime(ncid, "v", l, V)
!      U = mask_rho * U * 0.001 !scale factor of 0.001
!      V = mask_rho * V * 0.001 !scale factor of 0.001
  end subroutine read_current
 
  subroutine read_temp(l,TEMP)
    integer, intent(in) :: l
     real, dimension(:,:,:), intent(out) :: TEMP
    call getvartime(ncid, "temperature", l, TEMP)
!    TEMP = mask_rho * TEMP * 0.01 !scale factor of 0.01 
  end subroutine read_temp
!
  ! ------------------------------------------------
  !
  subroutine get_time(filepos, ocean_time)
    ! Return the time stamp of timestep=filepos in 
    ! a open input file

    integer, intent(in) :: filepos
    type(time_type), intent(out) :: ocean_time
     
    real(kind=8) :: otime
    call getvartime(ncid, "time", filepos, otime)
    if (time_units == "seconds") then
      otime = otime / (24*3600)
    else if (time_units == "hours") then
       otime = otime / 24
    end if
    ocean_time = time_origin + otime
    print *, 'ocean_time = ', ocean_time
  end subroutine get_time
  ! 
  ! ----------------------------------
  !
  subroutine close_input

    integer :: status

    status = nf_close(ncid)
    call check_err(status)

  end subroutine close_input

  !
  ! -----------------------------------
  !

end module input


